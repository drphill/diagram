package uk.vanleersum.diagramapp

import javax.swing.JMenuItem
import javax.swing.JPopupMenu

internal fun JPopupMenu.add(name: String, lambda: () -> Unit) {
    JMenuItem(name).let {
        it.addActionListener { lambda() }
        this.add(it)
    }
}
