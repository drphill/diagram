/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */
package uk.vanleersum.diagramapp

import org.apache.logging.log4j.kotlin.Logging
import uk.vanleersum.diagram.Diagram
import uk.vanleersum.diagram.DiagramPanel
import uk.vanleersum.diagram.DiagramViewer
import uk.vanleersum.diagram.elements.FontManager
import java.awt.BorderLayout
import java.awt.event.ComponentAdapter
import java.awt.event.ComponentEvent
import java.io.File
import javax.swing.JFileChooser
import javax.swing.JFrame
import javax.swing.JMenu
import javax.swing.JMenuBar
import javax.swing.JMenuItem
import javax.swing.SwingUtilities
import kotlin.system.exitProcess

internal class App : JFrame("State Transition Modeller"), Logging {

    internal var currentDiagram = Diagram()

    private val diagramPanelListener = object : DiagramPanel.Listener {
    }

    private val viewer = DiagramViewer(currentDiagram)

    init {
        logger.debug { "Debugging" }
        contentPane.add(viewer, BorderLayout.CENTER)
        viewer.panel.add(diagramPanelListener)
        viewer.editorDelegate = MyEditorDelegate(viewer)

        val menuBar = JMenuBar()
        val fileMenu = JMenu("File")
        menuBar.add(fileMenu)
        JMenuItem("Load").apply {
            addActionListener { load() }
            fileMenu.add(this)
        }
        JMenuItem("Save").apply {
            addActionListener { save() }
            fileMenu.add(this)
        }

        JMenuItem("Exit").apply {
            addActionListener { exitProcess(0) }
            fileMenu.add(this)
        }

        val viewMenu = JMenu("View")
        menuBar.add(viewMenu)
        val lineMenu = JMenu("Lines per sceeen")
        viewMenu.add(lineMenu)
        @Suppress("MagicNumber")
        for (linesPerScreen in 40..100 step 10) {
            JMenuItem("$linesPerScreen").apply {
                addActionListener { slf(linesPerScreen) }
                lineMenu.add(this)
            }
        }

        contentPane.add(menuBar, BorderLayout.NORTH)
        // panel.revalidate()

        addComponentListener(object : ComponentAdapter() {
            override fun componentShown(e: ComponentEvent?) {
                viewer.redraw()
            }
        })
        logger.debug { "Debugging" }
    }

    private val fc = JFileChooser(File("."))

    private fun load() {
        logger.debug { "load" }
    }

    private fun save() {
        if (JFileChooser.APPROVE_OPTION != fc.showSaveDialog(this)) return

        var file = fc.selectedFile
        logger.debug { file.path }
        if (!file.name.endsWith(".svg")) {
            file = File(file.path + ".svg")
        }
        currentDiagram.save(file, "A test comment with &strange <characters>")
    }

    private fun slf(n: Int) {
        FontManager.setUIFont(n)
        SwingUtilities.updateComponentTreeUI(this)
    }
}
