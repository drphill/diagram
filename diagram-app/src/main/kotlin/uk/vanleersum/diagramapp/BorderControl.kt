package uk.vanleersum.diagramapp
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import uk.vanleersum.diagram.ConnectionInfo
import uk.vanleersum.diagram.elements.area.AngledBorder
import uk.vanleersum.diagram.elements.area.AreaElement
import uk.vanleersum.diagram.elements.area.Border
import uk.vanleersum.diagram.elements.area.FlatBorder
import uk.vanleersum.diagram.elements.area.HBox
import uk.vanleersum.diagram.elements.area.RectangularPadding
import uk.vanleersum.diagram.elements.area.RoundedBorder
import uk.vanleersum.diagram.elements.area.Text
import uk.vanleersum.diagram.elements.area.VBox
import java.awt.Point
import javax.swing.JPopupMenu

internal class BorderControl(val delegate: MyEditorDelegate, name: String, val borderElement: Border) {
    companion object {
        fun flat(delegate: MyEditorDelegate, name: String) = BorderControl(delegate, name, FlatBorder(HBox()))
        fun rounded(delegate: MyEditorDelegate, name: String) = BorderControl(delegate, name, RoundedBorder(HBox()))
        fun angled(delegate: MyEditorDelegate, name: String) = BorderControl(delegate, name, AngledBorder(HBox()))
    }

    val title = Text(name)
    val titleBorder = RectangularPadding(title)
    val hbox = HBox()
    val vbox = VBox()
    val outerVBox = VBox(titleBorder, hbox).apply {
        borderElement.element = this
    }

    val children = mutableListOf<RectangularPadding>()

    var counter = 0

    @Suppress("LongMethod")
    fun popup(point: Point) = JPopupMenu().apply {
        val child = hbox.childAt(point)
        if (null == child) {
            add("Add Flat") {
                val b = RectangularPadding(
                    FlatBorder(RectangularPadding(Text("${title.string}-${counter++}")))
                )
                hbox.elements.add(b)
                vbox.elements.add(b)
                children.add(b)
                delegate.viewer.redraw()
            }
            add("Add Rounded") {
                val b = RectangularPadding(
                    RoundedBorder(RectangularPadding(Text("${title.string}-${counter++}")))
                )
                hbox.elements.add(b)
                vbox.elements.add(b)
                children.add(b)
                delegate.viewer.redraw()
            }
            add("Add Angled") {
                val b = RectangularPadding(
                    AngledBorder(RectangularPadding(Text("${title.string}-${counter++}")))
                )
                hbox.elements.add(b)
                vbox.elements.add(b)
                children.add(b)
                delegate.viewer.redraw()
            }
            if (outerVBox.elements.contains(hbox)) {
                add("Vertical") {
                    outerVBox.elements.remove(hbox)
                    outerVBox.elements.add(vbox)
                    delegate.viewer.redraw()
                }
            } else {
                add("Horizontal") {
                    outerVBox.elements.remove(vbox)
                    outerVBox.elements.add(hbox)
                    delegate.viewer.redraw()
                }
            }
            add("delete") {
                delegate.viewer.currentDiagram.removeLinesAttachedTo(borderElement)
                children.forEach {
                    delegate.viewer.currentDiagram.removeLinesAttachedTo(it.element)
                }
                // do this after letting the diagram tidy up.  Reason being that line elements use a weak
                // reference to their startElement and endElement. If we remove the last strong ref to
                // the start or end the diagram won't be able to identify lines to clear up.
                delegate.viewer.currentDiagram.remove(borderElement)
                children.clear()
                delegate.viewer.redraw()
            }
        } else {
            add("delete") {
                val el = children.firstOrNull { it.contains(point) }
                hbox.elements.removeIf { it.contains(point) }
                vbox.elements.removeIf { it.contains(point) }
                el?.let {
                    delegate.viewer.currentDiagram.removeLinesAttachedTo(it.element)
                }
                // do this after letting the diagram tidy up.  Reason being that line elements use a weak
                // reference to their startElement and endElement. If we remove the last strong ref to
                // the start or end the diagram won't be able to identify lines to clear up.
                children.remove(el)
                delegate.viewer.redraw()
            }
        }
    }

    fun connectionInfo(at: Point, element: AreaElement): ConnectionInfo {
        val child = childBorderAt(at)
        if (null == child) {
            return DefaultConnectionInfo(delegate, delegate.viewer.currentDiagram, element)
        } else {
            return DefaultConnectionInfo(delegate, delegate.viewer.currentDiagram, child)
        }
    }

    internal fun childBorderAt(at: Point) = children
        .firstOrNull { it.contains(at) }
        ?.element
}
