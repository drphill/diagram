package uk.vanleersum.diagramapp
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import org.apache.logging.log4j.kotlin.Logging
import uk.vanleersum.diagram.ConnectionInfo
import uk.vanleersum.diagram.DiagramViewer
import uk.vanleersum.diagram.EditorDelegate
import uk.vanleersum.diagram.elements.Element
import uk.vanleersum.diagram.elements.area.AreaElement
import uk.vanleersum.diagram.elements.line.LineElement
import java.awt.Point
import javax.swing.JPopupMenu

internal class MyEditorDelegate(internal val viewer: DiagramViewer) : EditorDelegate, Logging {
    private var counter = 0
    private val currentDiagram get() = viewer.currentDiagram

    internal val borderControls = mutableListOf<BorderControl>()

    override fun connectionInfo(at: Point, element: AreaElement): ConnectionInfo {
        val flat = borderControls.firstOrNull { it.borderElement.contains(at) }
        if (null != flat) {
            return flat.connectionInfo(at, element)
        }
        return DefaultConnectionInfo(this, currentDiagram, element)
    }

    override fun popup(point: Point, element: Element?): JPopupMenu {
        val flat = borderControls.firstOrNull { it.borderElement.contains(point) }
        if (null != flat) {
            return flat.popup(point)
        }

        return JPopupMenu().apply {
            when (element) {
                null -> backgroundPopup(point)
                is LineElement -> linePopup(element)
            }
        }
    }

    private fun JPopupMenu.linePopup(element: LineElement) {
        logger.debug { element.toString() }
        add("Delete") { currentDiagram.remove(element) }
    }

    private fun JPopupMenu.backgroundPopup(point: Point) {
        add("Flat") {
            val c = BorderControl.flat(this@MyEditorDelegate, "Flat-${counter++}")
            currentDiagram.add(c.borderElement)
            c.borderElement.centre = point
            viewer.redraw()
            borderControls.add(c)
        }
        add("Rounded") {
            val c = BorderControl.rounded(this@MyEditorDelegate, "Rounded-${counter++}")
            currentDiagram.add(c.borderElement)
            c.borderElement.centre = point
            viewer.redraw()
            borderControls.add(c)
        }
        add("Angled") {
            val c = BorderControl.angled(this@MyEditorDelegate, "Angled-${counter++}")
            currentDiagram.add(c.borderElement)
            c.borderElement.centre = point
            viewer.redraw()
            borderControls.add(c)
        }
    }

    override fun doubleClick(point: Point, element: Element?) {
        logger.debug { "double click" }
    }
}
