package uk.vanleersum.diagramapp
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import uk.vanleersum.diagram.ConnectionInfo
import uk.vanleersum.diagram.Diagram
import uk.vanleersum.diagram.elements.area.AreaElement
import uk.vanleersum.diagram.elements.line.LineArrow
import uk.vanleersum.diagram.elements.line.LineCircle
import uk.vanleersum.diagram.elements.line.LineElement
import java.awt.Color
import java.awt.Point

internal class DefaultConnectionInfo(
    val delegate: MyEditorDelegate,
    private val diagram: Diagram,
    override val sourceElement: AreaElement
) : ConnectionInfo {
    override var sinkElement: AreaElement? = null
    override var connectionColour = Color.RED
    override var highlightColour = ConnectionInfo.defaultDeclineHighlightColour

    override fun dragging(source: AreaElement, sourcePoint: Point, sink: AreaElement?, sinkPoint: Point) {
        val sinkControl = delegate.borderControls
            .firstOrNull { it.borderElement.contains(sinkPoint) }

        sinkElement = sinkControl?.childBorderAt(sinkPoint) ?: sink

        if (sourceElement.javaClass == sinkElement?.javaClass) {
            connectionColour = Color.GREEN
            highlightColour = ConnectionInfo.defaultAcceptHighlightColour
        } else {
            connectionColour = Color.RED
            highlightColour = ConnectionInfo.defaultDeclineHighlightColour
        }
    }

    override fun dropped(source: AreaElement, sourcePoint: Point, sink: AreaElement?, sinkPoint: Point) {
        val sourceControl = delegate.borderControls
            .firstOrNull { it.borderElement.contains(sinkPoint) }
        val sinkControl = delegate.borderControls
            .firstOrNull { it.borderElement.contains(sinkPoint) }
        if (source != sink && sink != null) {
            val line = LineElement().apply {
                startElement = sourceControl?.childBorderAt(sourcePoint) ?: sourceElement
                endElement = sinkControl?.childBorderAt(sinkPoint) ?: sink
                endArrow = LineArrow()
                startArrow = LineCircle()
                style = LineElement.MutableStyle(Color.BLACK)
            }
            diagram.add(line)
        }
    }
}
