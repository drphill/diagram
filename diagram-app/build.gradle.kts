/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

plugins {
    id("uk.vanleersum.kotlin.kotlin-application-conventions")
}

val artifactoryUrl: String by project
val artifactoryUsername: String by project
val artifactoryPassword: String by project

ext {
    val versionNumber: String by project
    val versionSuffix: String by project
    version = "${versionNumber}$versionSuffix"
}

repositories {
    mavenCentral()
    mavenLocal()

    maven {
        url = uri(artifactoryUrl)
        credentials {
            username = artifactoryUsername
            password = artifactoryPassword
        }
    }
}

dependencies {
    // implementation("uk.vanleersum:diagram:0.0.3")
    implementation(project(":diagram"))
}

application {
    // Define the main class for the application.
    mainClass.set("uk.vanleersum.diagramapp.EntryPoint")
    mainModule.set("uk.vanleersum.diagramapp")

    // https://bugs.openjdk.java.net/browse/JDK-8211302
    when {
        org.gradle.internal.os.OperatingSystem.current().isLinux ->
            applicationDefaultJvmArgs = listOf("-Djdk.gtk.version=2")
    }
}

// jlink {
//    addOptions("--strip-debug", "--compress", "2", "--no-header-files", "--no-man-pages")
//    mergedModule {
//        additive = true
//        requires("org.apache.logging.log4j")
//    }
//    forceMerge("kotlin")
//    forceMerge("slf4j")
//
//    imageName.set("StateTransitionModeller-$discriminator-$version")
//    println("1 " + imageName)
//
//    launcher {
//        name = "StateTransitionModeller-$discriminator"
//        noConsole = true
//        if (org.gradle.internal.os.OperatingSystem.current().isMacOsX) {
//            name = "$name.command"
//        } else if (org.gradle.internal.os.OperatingSystem.current().isLinux) {
//            name = "$name.sh"
//        }
//    }
//
//    jpackage {
//        imageName = "StateTransitionModeller-$discriminator"
//        skipInstaller = false
//        installerName = "StateTransitionModeller-$discriminator"
//        when {
//            org.gradle.internal.os.OperatingSystem.current().isWindows -> {
//                installerOptions =
//                    listOf("--win-menu", "--win-shortcut")
//            }
//            org.gradle.internal.os.OperatingSystem.current().isMacOsX -> {
//                appVersion = "1.0.0"
//            }
//        }
//    }
// }
