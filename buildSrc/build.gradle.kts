/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */
import java.io.*
import java.util.*

// Frig needed:
// https://stackoverflow.com/questions/70503915/specify-kotlin-version-in-buildsrc-kotlin-convention-plugin
// read gradle.properties programmatically
val props = Properties()
FileInputStream(file("../gradle.properties")).use {
    props.load(it)
}

plugins {
    `kotlin-dsl`
}

repositories {
    gradlePluginPortal()
}

dependencies {
    val kotlinVersion = props.getProperty("kotlinVersion")
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:${kotlinVersion}")

    implementation("com.pinterest:ktlint:0.40.0")
    // https://github.com/JLLeitschuh/ktlint-gradle
    // https://plugins.gradle.org/plugin/org.jlleitschuh.gradle.ktlint
    implementation("org.jlleitschuh.gradle:ktlint-gradle:10.0.0")

    implementation("io.gitlab.arturbosch.detekt:detekt-gradle-plugin:1.19.0")

    // https://github.com/java9-modularity/gradle-modules-plugin/blob/master/test-project-kotlin/build.gradle.kts
    implementation("org.javamodularity:moduleplugin:1.8.10")

    // https://mvnrepository.com/artifact/org.jacoco/jacoco-maven-plugin
    implementation("org.jacoco:jacoco-maven-plugin:0.8.8")

    // https://mvnrepository.com/artifact/org.sonarsource.scanner.gradle/sonarqube-gradle-plugin
    runtimeOnly("org.sonarsource.scanner.gradle:sonarqube-gradle-plugin:3.3")

    // https://mvnrepository.com/artifact/org.apache.maven.reporting/maven-reporting-api
    implementation("org.apache.maven.reporting:maven-reporting-api:4.0.0-M1")
}
