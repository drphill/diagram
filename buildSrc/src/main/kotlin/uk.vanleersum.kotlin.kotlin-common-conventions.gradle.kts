import org.jlleitschuh.gradle.ktlint.tasks.KtLintFormatTask
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

plugins {

    // Apply the org.jetbrains.kotlin.jvm Plugin to add support for Kotlin.
    id("org.jetbrains.kotlin.jvm")

    // https://plugins.gradle.org/plugin/org.jlleitschuh.gradle.ktlint
    id("org.jlleitschuh.gradle.ktlint")

    id("io.gitlab.arturbosch.detekt")

    id("org.javamodularity.moduleplugin")

    // provides code coverage metrics
    id("jacoco")

    // provides code quality assessments
    id("org.sonarqube")
}

repositories {
    // Use Maven Central for resolving dependencies.
    mavenCentral()
}

dependencies {
    constraints {
        // Define dependency versions as constraints
        implementation("org.apache.commons:commons-text:1.9")

        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    }

    // Align versions of all Kotlin components
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))

    // Use the Kotlin JDK 8 standard library.
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // Align versions of all Kotlin components
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))

    // Use JUnit Jupiter API for testing.
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.1")

    // Use JUnit Jupiter Engine for testing.
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")

    // https://logging.apache.org/log4j/kotlin/index.html
    // https://github.com/apache/logging-log4j-kotlin
    implementation("org.apache.logging.log4j:log4j-api-kotlin:1.0.0")
    implementation("org.apache.logging.log4j:log4j-api:2.17.2")
    implementation("org.apache.logging.log4j:log4j-core:2.17.2")
}

tasks.test {
    // Use junit platform for unit tests.
    useJUnitPlatform()
}

// Minimum jvmTarget of 1.8 needed since Kotlin 1.1
// Minimum version of 1.9 needed for modules
val targetLanguageLevel = "9"
tasks.compileKotlin {
    kotlinOptions.jvmTarget = targetLanguageLevel
    kotlinOptions.allWarningsAsErrors = true
}

tasks.compileTestKotlin {
    kotlinOptions.jvmTarget = targetLanguageLevel
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(true)
    }
}

tasks.compileJava {
    sourceCompatibility = targetLanguageLevel
    targetCompatibility = targetLanguageLevel
}

// https://docs.gradle.org/current/userguide/publishing_gradle_module_metadata.html#sub:disabling-gmm-publication
tasks.withType<GenerateModuleMetadata> {
    enabled = false
}

tasks.jar {
    exclude("*log4j2.xml")
}

ktlint {
    verbose.set(true)
    outputToConsole.set(true)
    coloredOutput.set(true)
    // Most of the rules are nicely implemented in the formatter, so
    // ktlintFormat sorts them out.  Unfortunately (and reasonably)
    // ktlint cannot expand wildcard imports, and these are allowed
    // by default in intellij.  So we can disable the rule here.
    disabledRules.set(setOf("no-wildcard-imports"))
    reporters {
        reporter(org.jlleitschuh.gradle.ktlint.reporter.ReporterType.HTML)
    }
    filter {
        exclude("**/style-violations.kt")
    }
}

// This bit of code fixes the warnings about ktLint plugin not declaring explicit dependencies
// between some tasks.  I kludged it together following hints so it maybe poor coding..... but
// we only need it until fix makes it into the plugin, and we use the new version
// Format task show deprecation messages in Gradle 7.0 #457
// https://github.com/JLLeitschuh/ktlint-gradle/issues/457
afterEvaluate {
    val f = tasks.withType<KtLintFormatTask>()
        .filter { it.name == "runKtlintFormatOverKotlinScripts" }
    tasks.withType<KtLintFormatTask>()
        .filter { it.name != "runKtlintFormatOverKotlinScripts" }
        .forEach { it.dependsOn(f) }
}
