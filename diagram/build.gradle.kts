
plugins {
    id("uk.vanleersum.kotlin.kotlin-library-conventions")
}

val artifactoryUrl: String by project
val artifactoryUsername: String by project
val artifactoryPassword: String by project

ext {
    val versionNumber: String by project
    val versionSuffix: String by project
    version = "${versionNumber}$versionSuffix"
}

repositories {
    mavenCentral()
    mavenLocal()

    maven {
        url = uri(artifactoryUrl)
        credentials {
            username = artifactoryUsername
            password = artifactoryPassword
        }
    }
}

dependencies {
    implementation("uk.vanleersum:ksvg:0.0.3")
}

publishing {
    repositories {
        maven {
            url = uri(artifactoryUrl)
            credentials {
                username = artifactoryUsername
                password = artifactoryPassword
            }
        }
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = "uk.vanleersum"
            artifactId = "diagram"

            from(components["java"])
        }
    }
}
