package uk.vanleersum.diagram.elements

import org.apache.logging.log4j.kotlin.Logging
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import uk.vanleersum.diagram.elements.geometry.Line
import java.awt.Point

internal class LineTest : Logging {

    @Test
    fun intersection() {
        logger.debug { "test" }
        val horizontal = Line(Point(10, 10), Point(20, 10))
        val vertical = Line(Point(15, 5), Point(15, 25))
        Assertions.assertEquals(horizontal.intersection(vertical), Point(15, 10))
        Assertions.assertEquals(vertical.intersection(horizontal), Point(15, 10))

        val verticalMiss = Line(Point(5, 5), Point(5, 25))
        Assertions.assertEquals(horizontal.intersection(verticalMiss), Point(5, 10))
        Assertions.assertEquals(verticalMiss.intersection(horizontal), Point(5, 10))
    }

    @Test
    fun boundedIntersection() {
        val horizontal = Line(Point(10, 10), Point(20, 10))
        val vertical = Line(Point(15, 5), Point(15, 25))
        val boundedIntersection = horizontal.boundedIntersection(vertical)
        Assertions.assertEquals(boundedIntersection, Point(15, 10))
        Assertions.assertEquals(vertical.boundedIntersection(horizontal), Point(15, 10))

        val verticalMiss = Line(Point(5, 5), Point(5, 25))
        Assertions.assertEquals(horizontal.boundedIntersection(verticalMiss), null)
        Assertions.assertEquals(verticalMiss.boundedIntersection(horizontal), null)
    }
}
