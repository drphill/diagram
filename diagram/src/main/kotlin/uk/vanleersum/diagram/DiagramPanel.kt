/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */
package uk.vanleersum.diagram

import org.apache.logging.log4j.kotlin.Logging
import uk.vanleersum.diagram.editor.DiagramEditor
import uk.vanleersum.diagram.elements.Element
import uk.vanleersum.diagram.elements.GraphicsRenderer
import java.awt.Dimension
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.Point
import java.awt.event.MouseEvent
import java.awt.event.MouseListener
import java.awt.event.MouseMotionListener
import java.awt.geom.AffineTransform
import java.lang.Integer.max
import javax.swing.JPanel
import javax.swing.Scrollable
import javax.swing.TransferHandler

/**
 * A [JPanel] that can be used to render a [Diagram].
 */
public class DiagramPanel(
    private val scrollableAdapter: ScrollableAdapter = ScrollableAdapter()
) : JPanel(), Scrollable by scrollableAdapter, Logging {
    public interface Listener {
        /**
         * user pressed mouse on element
         */
        public fun touched(element: Element?, point: Point) {}

        /**
         * mouse over element
         */
        public fun over(element: Element?, point: Point) {}
    }

    private val listeners = mutableListOf<Listener>()
    public fun add(listener: Listener): Boolean = listeners.add(listener)
    public fun remove(listener: Listener): Boolean = listeners.remove(listener)

    internal var scale = 1.0
    // easiest way to modify the event point is to translate it in place.  Note
    // that this modifies the point itself - a useful thing to do since other subscribers
    // to events on here will get the modified value.
    private fun scaled(e: MouseEvent): MouseEvent = e.apply {
        translatePoint((e.x / scale).toInt() - e.x, (e.y / scale).toInt() - e.y)
    }
    public fun unscaled(point: Point): Point = Point((point.x * scale).toInt(), (point.y * scale).toInt())
    private val editor: DiagramEditor = DiagramEditor(this)

    /**
     * Setting a non-null value will allow editing the diagram.  Setting a non-null
     * value will disallow editing the diagram.  The default is null.
     */
    public var editorDelegate: EditorDelegate?
        get() = editor.editorDelegate
        set(value) {
            editor.editorDelegate = value
        }

    /**
     * The diagram rendered by the receiver.
     */
    public var diagram: Diagram = Diagram()
        set(value) {
            field = value
            diagram.calculate()
            editor.reset()
            diagram.internalListener = Diagram.InternalListener { redraw() }
        }

    init {
        isDoubleBuffered = true
        // background = Color.WHITE
        // NOTE: the use of scaled function changes the event point for all users.
        // We have to make sure that this listener is the first installed.
        addMouseMotionListener(object : MouseMotionListener {
            override fun mouseDragged(e: MouseEvent) { scaled(e) }
            override fun mouseMoved(e: MouseEvent) {
                scaled(e)
                listeners.forEach { it.over(diagram[e.point], e.point) }
            }
        })

        addMouseListener(object : MouseListener {
            override fun mouseClicked(e: MouseEvent) { scaled(e) }
            override fun mousePressed(e: MouseEvent) {
                scaled(e)
                listeners.forEach { it.touched(diagram[e.point], e.point) }
            }
            override fun mouseReleased(e: MouseEvent) { scaled(e) }
            override fun mouseEntered(e: MouseEvent) { scaled(e) }
            override fun mouseExited(e: MouseEvent) { scaled(e) }
        })

        transferHandler = object : TransferHandler() {
            override fun canImport(support: TransferSupport?): Boolean {
                support ?: return false
                val dropPoint = support.dropLocation.dropPoint
                val scaledPoint = Point((dropPoint.x / scale).toInt(), (dropPoint.y / scale).toInt())
                // logger.debug { "drop: ${dropPoint.x}, ${dropPoint.y}; scaled: ${scaledPoint.x}, ${scaledPoint.y}" }
                return editorDelegate?.canImport(support, scaledPoint) ?: false
            }

            override fun importData(support: TransferSupport?): Boolean {
                support ?: return false
                val dropPoint = support.dropLocation.dropPoint
                val scaledPoint = Point((dropPoint.x / scale).toInt(), (dropPoint.y / scale).toInt())
                // logger.debug { "drop: ${dropPoint.x}, ${dropPoint.y}; scaled: ${scaledPoint.x}, ${scaledPoint.y}" }
                return editorDelegate?.importData(support, scaledPoint) ?: false
            }
        }
    }

    internal var parentSize: Dimension = Dimension()

    override fun getPreferredSize(): Dimension {
        val physicalWidth = (diagram.size.width * scale).toInt()
        val physicalHeight = (diagram.size.height * scale).toInt()
        return Dimension(
            max(parentSize.width, physicalWidth),
            max(parentSize.height, physicalHeight)
        )
    }

    override fun paintComponent(g: Graphics) {
        super.paintComponent(g)
        if (g is Graphics2D) {
            val saveAT: AffineTransform = g.transform
            g.scale(scale, scale)
            val renderer = GraphicsRenderer(g)
            diagram.render(renderer)
            editor.render(renderer)
            // Restore original transform
            g.transform = saveAT
        }
    }

    override fun getPreferredScrollableViewportSize(): Dimension = preferredSize

    public fun redraw() {
        diagram.calculate()
        editor.calculate()
        repaint()
        revalidate()
    }
}
