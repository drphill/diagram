package uk.vanleersum.diagram
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import org.apache.logging.log4j.kotlin.Logging
import uk.vanleersum.diagram.editor.selected
import uk.vanleersum.diagram.elements.Element
import uk.vanleersum.diagram.elements.Renderer
import uk.vanleersum.diagram.elements.SvgRenderer
import uk.vanleersum.diagram.elements.area.AreaElement
import uk.vanleersum.diagram.elements.line.LineElement
import java.awt.Color
import java.awt.Dimension
import java.awt.Point
import java.awt.font.FontRenderContext
import java.awt.geom.AffineTransform
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter

/**
 * A collection of [Element] that can be rendered to a [DiagramPanel]
 */
@Suppress("TooManyFunctions")
public class Diagram : Iterable<Element>, Logging {
    /**
     * Internal only change listening interface
     */
    internal fun interface InternalListener {
        fun changed()
    }
    internal var internalListener: InternalListener? = null

    private val elements = mutableListOf<Element>()

    public fun add(element: Element) {
        elements.add(element)
        calculate()
        internalListener?.changed()
    }

    public fun remove(element: Element) {
        elements.remove(element)
        elements.removeIf {
            it is LineElement && (it.start == element || it.end == element)
        }
        calculate()
        internalListener?.changed()
    }

    public fun removeLinesAttachedTo(element: AreaElement) {
        // elements.mapNotNull { it as? LineElement }

        elements.removeIf {
            true == (it as? LineElement)?.let { it.startElement == element || it.endElement == element }
        }
    }

    public fun clear() { elements.clear() }
    public fun removeAll(elements: Iterable<Element>) {
        this.elements.removeAll(elements)
    }
    public fun addAll(elements: Iterable<Element>) {
        this.elements.addAll(elements)
    }

    public fun redraw() {
        calculate()
        internalListener?.changed()
    }

    internal operator fun get(point: Point): Element? = elements.firstOrNull { it.contains(point) }

    internal var fontRenderContext = FontRenderContext(
        AffineTransform(), true, false
    )

    public val size: Dimension
        get() {
            val w = elements.maxOfOrNull { it.max.x } ?: minWidth
            val h = elements.maxOfOrNull { it.max.y } ?: minHeight
            return Dimension(w + defaultMargin, h + defaultMargin)
        }

    internal fun calculate() {
        elements.toList().forEach { it.calculate(fontRenderContext) }
    }

    internal fun render(renderer: Renderer) {
        calculate()
        val areas = elements.mapNotNull { it as? AreaElement }
        val lines = elements.mapNotNull { it as? LineElement }
        areas.filterNot { it.selected }.forEach { it.render(renderer) }
        areas.filter { it.selected }.forEach { it.render(renderer) }
        lines.filterNot { it.selected }.forEach { it.render(renderer) }
        lines.filter { it.selected }.forEach { it.render(renderer) }
    }

    override fun iterator(): Iterator<Element> = elements.iterator()

    public fun save(file: File) {
        BufferedWriter(FileWriter(file)).use { writer ->
            SvgRenderer(
                writer,
                size.width, size.height,
                backgroundColour
            ).use { svgWriter ->
                calculate()
                elements.forEach { it.render(svgWriter) }
            }
        }
    }

    public fun save(file: File, comment: String) {
        calculate()
        BufferedWriter(FileWriter(file)).use { writer ->
            SvgRenderer(
                writer,
                size.width, size.height,
                backgroundColour
            ).use { svgWriter ->
                calculate()
                elements.forEach { it.render(svgWriter) }
                svgWriter.comment(comment)
            }
        }
    }

    public companion object {
        internal val backgroundColour = Color(240, 240, 240)
        private const val minWidth = 200
        private const val minHeight = 200
        private const val defaultMargin = 30
    }
}
