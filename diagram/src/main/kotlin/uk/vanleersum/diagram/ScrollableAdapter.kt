package uk.vanleersum.diagram
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */
import java.awt.Dimension
import java.awt.Rectangle
import javax.swing.Scrollable

public class ScrollableAdapter(
    public var preferredSize: Dimension = Dimension(),
    public var unitIncrement: Int = scrollableUnitIncrement,
    public var blockIncrement: Int = scrollableBlockIncrement,
    public var tracksViewportWidth: Boolean = false,
    public var tracksViewportHeight: Boolean = false,
) : Scrollable {
    override fun getPreferredScrollableViewportSize(): Dimension = preferredSize

    override fun getScrollableUnitIncrement(
        visibleRect: Rectangle?,
        orientation: Int,
        direction: Int
    ): Int = unitIncrement

    override fun getScrollableBlockIncrement(
        visibleRect: Rectangle?,
        orientation: Int,
        direction: Int
    ): Int = blockIncrement

    override fun getScrollableTracksViewportWidth(): Boolean = tracksViewportWidth

    override fun getScrollableTracksViewportHeight(): Boolean = tracksViewportHeight

    public companion object {
        public const val scrollableUnitIncrement: Int = 10
        public const val scrollableBlockIncrement: Int = 10
    }
}
