package uk.vanleersum.diagram
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */
import uk.vanleersum.diagram.elements.Element
import uk.vanleersum.diagram.elements.area.AreaElement
import java.awt.Point
import javax.swing.JPopupMenu
import javax.swing.TransferHandler

public interface EditorDelegate {
    public fun connectionInfo(at: Point, element: AreaElement): ConnectionInfo?
    public fun popup(point: Point, element: Element?): JPopupMenu?
    public fun doubleClick(point: Point, element: Element?)

    /**
     * Forwards DragNDrop support to the delegate.  For more information
     * see [TransferHandler] and its associated documentation.
     * There is a good tutorial here: https://docs.oracle.com/javase/tutorial/uiswing/dnd/index.html
     *
     * Since the Editor panel may have been scaled the [TransferHandler.TransferSupport.getDropPoint()]
     * may not correctly represent the model/logical coordinates.  The model/logical point
     * represented by the drop point is supplied as an extra argument.
     */
    public fun canImport(
        support: TransferHandler.TransferSupport?,
        scaledDropPoint: Point
    ): Boolean = false

    /**
     * Forwards DragNDrop support to the delegate.  For more information
     * see [TransferHandler] and its associated documentation.
     * There is a good tutorial here: https://docs.oracle.com/javase/tutorial/uiswing/dnd/index.html
     *
     * Since the Editor panel may have been scaled the [TransferHandler.TransferSupport.getDropPoint()]
     * may not correctly represent the model/logical coordinates.  The model/logical point
     * represented by the drop point is supplied as an extra argument.
     */
    public fun importData(
        support: TransferHandler.TransferSupport?,
        scaledDropPoint: Point
    ): Boolean = false
}
