package uk.vanleersum.diagram.elements.area

import org.apache.logging.log4j.kotlin.Logging
import uk.vanleersum.diagram.elements.Renderer
import uk.vanleersum.diagram.elements.ScaleMode
import uk.vanleersum.ksvg.svg.SVG
import java.awt.Point
import java.awt.font.FontRenderContext

public class Svg(
    public var svgText: String,
    public override var width: Int,
    public override var height: Int,
    public var scaleMode: ScaleMode
) : AreaElement, Logging {

    override var orthogonalIntersect: Boolean = false
    override var x: Int = 0
    override var y: Int = 0

    override fun childAt(point: Point): AreaElement? {
        return if (contains(point)) {
            this
        } else {
            null
        }
    }

    override fun calculate(context: FontRenderContext) {
        // nothing to do
    }

    override fun render(renderer: Renderer) {
        val svg = SVG.from(svgText) ?: return
        renderer.renderSvg(svg, bounds, scaleMode)
    }
}
