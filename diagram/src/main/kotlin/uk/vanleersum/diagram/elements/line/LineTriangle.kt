/*******************************************************************************
 * Copyright (C) 2017 Dr Phill van Leersum
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http:></http:>//www.gnu.org/licenses/>.
 */
package uk.vanleersum.diagram.elements.line

import uk.vanleersum.diagram.elements.Renderer
import java.awt.Point
import java.awt.font.FontRenderContext

/**
 * Add a triangle to a line such that the arrow automatically adjusts its
 position and orientation when the line changes
 * @author phill
 */
@Suppress("MagicNumber")
public class LineTriangle(
    public var length: Int = 12,
    public var flare: Double = 0.4
) : LineDecoration {
    public val xpoints: IntArray = intArrayOf(0, 0, 0)
    public val ypoints: IntArray = intArrayOf(0, 0, 0)

    override fun calculate(src: Point, dst: Point) {

        val srcX: Double = src.getX()
        val dstX: Double = dst.getX()
        val srcY: Double = src.getY()
        val dstY: Double = dst.getY()
        val x = DoubleArray(3)
        val y = DoubleArray(3)
        var deltaX = srcX - dstX
        var deltaY = srcY - dstY
        // calculate length of line between points
        val lineLength = Math.sqrt(deltaX * deltaX + deltaY * deltaY)
        // what proportion of this do we want?
        var shapeLengthLineLengthRatio = this.length / lineLength // small arrow length is
        // 20?
        for (i in 0..2) {
            x[i] = dstX
            y[i] = dstY
        }
        x[2] = dstX + shapeLengthLineLengthRatio * deltaX * 2
        x[1] = x[2]
        y[2] = dstY + shapeLengthLineLengthRatio * deltaY * 2
        y[1] = y[2]
        shapeLengthLineLengthRatio *= this.flare
        val temp = deltaX
        deltaX = deltaY
        deltaY = temp
        x[1] += shapeLengthLineLengthRatio * deltaX
        y[1] -= shapeLengthLineLengthRatio * deltaY
        x[2] -= shapeLengthLineLengthRatio * deltaX
        y[2] += shapeLengthLineLengthRatio * deltaY
        for (i in 0..2) {
            this.xpoints[i] = x[i].toInt()
            this.ypoints[i] = y[i].toInt()
        }
    }

    override fun calculate(context: FontRenderContext) {
        // irrelevant
    }

    override fun render(renderer: Renderer) {
        renderer.strokeWidth = 1f
        val pts = xpoints.zip(ypoints).map { Point(it.first, it.second) }
        renderer.polygon(pts)
    }
}
