package uk.vanleersum.diagram.elements.line
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import uk.vanleersum.diagram.elements.Renderer
import java.awt.Point
import java.awt.font.FontRenderContext

@Suppress("MagicNumber")
public class LineArrow(
    public var length: Int = 12,
    public var flare: Double = 0.4
) : LineDecoration {
    public val xpoints: IntArray = intArrayOf(0, 0, 0, 0)
    public val ypoints: IntArray = intArrayOf(0, 0, 0, 0)

    public override fun calculate(src: Point, dst: Point) {

        val srcX = src.x.toDouble()
        val dstX = dst.x.toDouble()
        val srcY = src.y.toDouble()
        val dstY = dst.y.toDouble()

        val x = DoubleArray(xpoints.size)
        val y = DoubleArray(ypoints.size)

        var deltaX = srcX - dstX
        var deltaY = srcY - dstY
        // calculate length of line between points
        val lineLength = Math.sqrt(deltaX * deltaX + deltaY * deltaY)
        // what proportion of this do we want?
        var shapeLengthLineLengthRatio = this.length / lineLength

        for (i in 0..3) {
            x[i] = dstX
            y[i] = dstY
        }

        x[2] += shapeLengthLineLengthRatio * deltaX * 1.5
        y[2] += shapeLengthLineLengthRatio * deltaY * 1.5

        /*
		 * this is the 'back point' of arrow on line. We wish to construct an
		 * imaginary line through this orthogonal to the original line and
		 * offset along it in both directions to determine the outer point
		 * of the arrow
		 */
        x[3] = dstX + shapeLengthLineLengthRatio * deltaX * 2.0
        x[1] = x[3]
        y[3] = dstY + shapeLengthLineLengthRatio * deltaY * 2.0
        y[1] = y[3]

        /*
		 * with an orthogonal line the x and y deltas are exchanged but the
		 * length will remain the same, so the ratios will be the same but we
		 * wish to reduce arrow angle
		 */
        shapeLengthLineLengthRatio *= this.flare
        val temp = deltaX
        deltaX = deltaY
        deltaY = temp
        x[1] += shapeLengthLineLengthRatio * deltaX
        y[1] -= shapeLengthLineLengthRatio * deltaY
        x[3] -= shapeLengthLineLengthRatio * deltaX
        y[3] += shapeLengthLineLengthRatio * deltaY

        for (i in 0..3) {
            this.xpoints[i] = x[i].toInt()
            this.ypoints[i] = y[i].toInt()
        }
    }

    override fun calculate(context: FontRenderContext) {
        // irrelevant
    }

    override fun render(renderer: Renderer) {
        renderer.strokeWidth = 1f
        val pts = xpoints.zip(ypoints).map { Point(it.first, it.second) }
        renderer.polygon(pts)
    }
}
