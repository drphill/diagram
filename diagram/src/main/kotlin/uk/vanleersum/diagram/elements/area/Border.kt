package uk.vanleersum.diagram.elements.area

import java.awt.Color

public interface Border : AreaElement {
    public var element: AreaElement
    public interface Style {
        public val borderColour: Color?
        public val backgroundColour: Color
        public val strokeWidth: Float
    }

    public data class MutableStyle(
        override var borderColour: Color? = defaultStyle.borderColour,
        override var backgroundColour: Color = defaultStyle.backgroundColour,
        override var strokeWidth: Float = defaultStyle.strokeWidth
    ) : Style

    public var style: Style

    public companion object {
        public val defaultStyle: Style = object : Style {
            override val borderColour: Color = Color.BLACK
            override val backgroundColour: Color = Color.WHITE
            override val strokeWidth: Float = 1f
        }
    }
}
