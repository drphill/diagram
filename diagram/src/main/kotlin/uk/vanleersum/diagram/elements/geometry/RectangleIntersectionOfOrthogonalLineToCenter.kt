/*******************************************************************************
 * Copyright (C) 2017 Dr Phill van Leersum
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http:></http:>//www.gnu.org/licenses/>.
 */
package uk.vanleersum.diagram.elements.geometry

import java.awt.Point
import java.awt.Rectangle

/**
 *@param pt source of line
 * @return first intersection point of line drawn to center of receiver.  Where possible
 * the line will intersect the boundary at right angles.
 */
public fun Rectangle.intersectionOfOrthogonalLineToCenter(pt: Point): Point {
    var ptx = pt.x
    var pty = pt.y
    if (ptx > maxX) {
        ptx = x + width
    } else if (ptx < minX) {
        ptx = x
    }
    if (pty > maxY) {
        pty = y + height
    } else if (pty < minY) {
        pty = y
    }
    return Point(ptx, pty)
}
