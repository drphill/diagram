package uk.vanleersum.diagram.elements
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import org.apache.logging.log4j.kotlin.Logging
import uk.vanleersum.ksvg.svg.SvgElement
import java.awt.BasicStroke
import java.awt.Color
import java.awt.Font
import java.awt.Graphics2D
import java.awt.Point
import java.awt.Rectangle
import kotlin.math.max
import kotlin.math.min

public class GraphicsRenderer(private val graphics: Graphics2D) : Renderer, Logging {
    override var strokeWidth: Float = 1f
    override var fillColour: Color? = null
    override var italic: Boolean = false
    override var lineDash: Iterable<Int>? = null
    override var strokeColour: Color? = null

    override fun comment(string: String) {
        // Comments are not relevent
    }

    private val lineDashFloats get() = lineDash?.map { it.toFloat() }?.toFloatArray()

    private val basicStroke get() = BasicStroke(
        strokeWidth, BasicStroke.CAP_ROUND,
        BasicStroke.JOIN_ROUND, 1.0f,
        lineDashFloats, 0.0f
    )

    override fun rect(x: Int, y: Int, width: Int, height: Int) {
        if (null != fillColour) {
            graphics.color = fillColour
            graphics.fillRect(x, y, width, height)
        }
        if (null != strokeColour) {
            graphics.color = strokeColour
            graphics.stroke = basicStroke
            graphics.drawRect(x, y, width, height)
        }
    }

    override fun oval(x: Int, y: Int, width: Int, height: Int) {
        if (null != fillColour) {
            graphics.color = fillColour
            graphics.fillOval(x, y, width, height)
        }
        if (null != strokeColour) {
            graphics.color = strokeColour
            graphics.stroke = basicStroke
            graphics.drawOval(x, y, width, height)
        }
    }

    override fun roundedRect(x: Int, y: Int, width: Int, height: Int, xRad: Int, yRad: Int) {
        if (null != fillColour) {
            graphics.color = fillColour
            graphics.fillRoundRect(x, y, width, height, xRad * 2, yRad * 2)
        }
        if (null != strokeColour) {
            graphics.color = strokeColour
            graphics.stroke = basicStroke
            graphics.drawRoundRect(x, y, width, height, xRad * 2, yRad * 2)
        }
    }

    override fun textInRect(text: String, x: Int, y: Int, width: Int, height: Int, font: Font) {
        graphics.font = font
        graphics.color = fillColour
        graphics.drawString(text, x, y + graphics.fontMetrics.ascent)
    }

    override fun line(x1: Int, y1: Int, x2: Int, y2: Int) {
        if (null != strokeColour) {
            graphics.color = strokeColour
            graphics.stroke = basicStroke
            graphics.drawLine(x1, y1, x2, y2)
        }
    }

    override fun polygon(points: Iterable<Point>) {
        val xPoints = points.map { it.x }.toIntArray()
        val yPoints = points.map { it.y }.toIntArray()
        if (null != fillColour) {
            graphics.color = fillColour
            graphics.fillPolygon(xPoints, yPoints, xPoints.size)
        }
        if (null != strokeColour) {
            graphics.color = strokeColour
            graphics.stroke = basicStroke
            graphics.drawPolygon(xPoints, yPoints, xPoints.size)
        }
    }

    override fun renderSvg(svg: SvgElement, rectangle: Rectangle, scaleMode: ScaleMode) {
        val widthRatio = rectangle.width / svg.width.toDouble()
        val heightRatio = rectangle.height / svg.height.toDouble()
        val scale = when (scaleMode) {
            ScaleMode.None -> 1.0
            ScaleMode.FitHeight -> heightRatio
            ScaleMode.FitWidth -> widthRatio
            ScaleMode.AspectFit -> min(widthRatio, heightRatio)
            ScaleMode.Fill -> max(widthRatio, heightRatio)
        }
        val saveTransform = graphics.transform
        val saveClip = graphics.clip
        graphics.clip(rectangle)
        graphics.translate(rectangle.x, rectangle.y)
        graphics.scale(scale, scale)
        svg.render(graphics)
        graphics.transform = saveTransform
        graphics.clip = saveClip
    }

    override fun close() {
        // not relevant
    }
}
