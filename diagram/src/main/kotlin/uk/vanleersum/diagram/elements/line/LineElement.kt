package uk.vanleersum.diagram.elements.line
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import uk.vanleersum.diagram.editor.DiagramEditor
import uk.vanleersum.diagram.editor.selected
import uk.vanleersum.diagram.elements.Element
import uk.vanleersum.diagram.elements.Renderer
import uk.vanleersum.diagram.elements.area.AreaElement
import uk.vanleersum.diagram.elements.geometry.Line
import uk.vanleersum.diagram.elements.geometry.midway
import java.awt.Color
import java.awt.Point
import java.awt.Rectangle
import java.awt.font.FontRenderContext
import java.lang.ref.WeakReference

/**
 * LineElement with specified start and end points.
 * Optional start and/or end [AreaElement]s.  If specified then these are used to
 * calculate the line ends.  The references to the start and endElements are weak
 * to help diagram cleanup.
 */
@Suppress("TooManyFunctions")
public class LineElement : Element {
    public interface Style {
        public val hitAccuracy: Int
        public val colour: Color?
        public val stroke: Float
        public val lineDashArray: FloatArray?
    }

    public class MutableStyle(
        override val colour: Color? = Color.BLACK,
        override val stroke: Float = 1f,
        override val lineDashArray: FloatArray? = null,
        override var hitAccuracy: Int = LineElement.hitAccuracy,
    ) : Style

    public var style: Style = MutableStyle()

    public var start: Point = Point()
    public var end: Point = Point()
    public var startArrow: LineDecoration? = null
    public var endArrow: LineDecoration? = null
    public val waypoints: MutableList<Point> = mutableListOf<Point>()
    private var weakStart: WeakReference<AreaElement>? = null
    private var weakEnd: WeakReference<AreaElement>? = null

    private val _labels = mutableListOf<LineLabel>()
    public val labels: List<AreaElement> get() = _labels.map { it.element }
    public fun addLabel(element: AreaElement): LineLabel = LineLabel(this, element).also { _labels.add(it) }
    public fun removeLabel(element: AreaElement): Boolean = _labels.removeIf { it.element == element }

    public val centre: Point get() {
        return when {
            waypoints.isEmpty() -> Point((start.x + end.x) / 2, (start.y + end.y) / 2)
            waypoints.size % 2 == 0 -> {
                val i = waypoints.size / 2
                waypoints[i - 1] midway waypoints[i]
            }
            else -> waypoints[waypoints.size / 2]
        }
    }

    override fun isContainedWithin(rectangle: Rectangle): Boolean {
        return allPoints.all { rectangle.contains(it) }
    }

    override fun move(dx: Int, dy: Int) {
        waypoints.forEach { it.translate(dx, dy) }
    }

    public var startElement: AreaElement?
        get() = weakStart?.get()
        set(value) { weakStart = WeakReference(value) }

    public var endElement: AreaElement?
        get() = weakEnd?.get()
        set(value) { weakEnd = WeakReference(value) }

    override fun calculate(context: FontRenderContext) {
        val second = waypoints.firstOrNull() ?: end
        val penultmate = waypoints.lastOrNull() ?: start
        end = endElement?.intersectFrom(penultmate) ?: end
        start = startElement?.intersectFrom(second) ?: start
        if (waypoints.isEmpty()) {
            end = endElement?.intersectFrom(start) ?: end
            start = startElement?.intersectFrom(end) ?: start
        }
        startArrow?.calculate(second, start)
        endArrow?.calculate(penultmate, end)

        _labels.forEach {
            it.calculate(context)
        }
    }

    override fun contains(point: Point): Boolean {
        return segments.any { (start, end) ->
            val line = Line(start, end)

            line.isWithinBounds(point, style.hitAccuracy) &&
                line.distance(point) < style.hitAccuracy
        } || labels.any { it.contains(point) } ||
            waypoints.any { waypointHit(point, it) }
    }

    override val max: Point
        get() = Point(allPoints.maxOf { it.x }, allPoints.maxOf { it.y })

    override fun render(renderer: Renderer) {
        if (selected) {
            DiagramEditor.glowColour.forEachIndexed { index, color ->
                renderer.strokeWidth = index.toFloat()
                renderer.strokeColour = color
                renderer.lineDash = null
                segments.forEach { (start, end) ->
                    renderer.line(start.x, start.y, end.x, end.y)
                }
            }
        }
        renderer.fillColour = style.colour
        renderer.lineDash = style.lineDashArray?.map { it.toInt() }
        renderer.stroke(style.colour)
        renderer.strokeWidth = style.stroke
        segments.forEach { (start, end) ->
            renderer.line(start.x, start.y, end.x, end.y)
        }
        labels.forEach { it.render(renderer) }

        if (selected) {
            waypoints.forEach {
                renderer.strokeWidth = style.stroke
                renderer.fillColour = DiagramEditor.selectionColour
                renderer.strokeColour = style.colour
                renderer.oval(
                    it.x - style.hitAccuracy, it.y - style.hitAccuracy,
                    2 * style.hitAccuracy, 2 * style.hitAccuracy
                )
            }
        }

        endArrow?.render(renderer)
        startArrow?.render(renderer)
    }

    private val allPoints get() = waypoints.toMutableList().apply {
        add(0, start)
        add(end)
    }

    private val segments: List<Pair<Point, Point>> get() = allPoints.zipWithNext()

    private fun waypointHit(point: Point, waypoint: Point): Boolean = point.distance(waypoint) <= hitAccuracy

    internal fun labelHit(point: Point) = _labels.firstOrNull { it.contains(point) }

    internal fun waypoint(point: Point) = waypoints.firstOrNull { waypointHit(point, it) }

    internal fun dragStart(point: Point): Int {
        // need to test for existing waypoint
        val existingWaypoint = waypoints.indexOfFirst { waypointHit(point, it) }
        if (existingWaypoint > -1) {
            return existingWaypoint
        }
        val index = segments.indexOfFirst { (start, end) ->
            Line(start, end).distance(point) < style.hitAccuracy
        }
        return if (index > -1) {
            waypoints.add(index, point)
            index
        } else {
            waypoints.add(point)
            waypoints.lastIndex
        }
    }

    public companion object {
        public const val hitAccuracy: Int = (DiagramEditor.selectionWidth / 2).toInt()
    }
}
