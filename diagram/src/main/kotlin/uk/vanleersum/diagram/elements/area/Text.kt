package uk.vanleersum.diagram.elements.area
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import org.apache.logging.log4j.kotlin.Logging
import uk.vanleersum.diagram.elements.FontManager
import uk.vanleersum.diagram.elements.Renderer
import uk.vanleersum.diagram.elements.geometry.intersectionOfLineToCenter
import uk.vanleersum.diagram.elements.geometry.intersectionOfOrthogonalLineToCenter
import java.awt.Color
import java.awt.Font
import java.awt.Point
import java.awt.font.FontRenderContext
import java.awt.font.TextLayout

public class Text(public var string: String) : AreaElement, Logging {
    public var font: Font = FontManager.defaultFont
    public var textColour: Color = Color.BLACK
    override var x: Int = 0
    override var y: Int = 0

    override fun childAt(point: Point): AreaElement? {
        return if (contains(point)) {
            this
        } else {
            null
        }
    }

    override var width: Int = 0
        private set
    override var height: Int = 0
        private set
    public var italic: Boolean = false
    private var ascent = 0
    override var orthogonalIntersect: Boolean = true

    override fun intersectFrom(point: Point): Point = if (orthogonalIntersect) {
        bounds.intersectionOfOrthogonalLineToCenter(point)
    } else {
        bounds.intersectionOfLineToCenter(point)
    }

    override fun calculate(context: FontRenderContext) {
        ascent = 0
        height = 0
        width = 0
        if (string.isNotBlank()) {
            val tl = TextLayout(string, font, context)
            ascent = tl.baseline.toInt()
            val b = tl.bounds
            height = (tl.ascent + tl.leading + tl.descent).toInt() + 1
            width = (b.width + b.x).toInt()
        }
    }

    override fun render(renderer: Renderer) {
        if (string.isNotBlank()) {
            renderer.italic = this.italic
            renderer.fillColour = textColour
            // Font is placed a little high in Intellij built-in viewer
            renderer.textInRect(string, x, y, width, height, font)
        }
    }
}
