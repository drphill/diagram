package uk.vanleersum.diagram.elements
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import uk.vanleersum.ksvg.svg.SvgElement
import java.awt.Color
import java.awt.Font
import java.awt.Point
import java.awt.Rectangle
import java.io.Closeable

public interface Renderer : Closeable {
    public var strokeWidth: Float
    public var fillColour: Color?
    public var italic: Boolean
    public var lineDash: Iterable<Int>?
    public var strokeColour: Color?

    public fun comment(string: String)
    public fun stroke(colour: Color?) { strokeColour = colour }

    public fun rect(x: Int, y: Int, width: Int, height: Int)

    @Suppress("LongParameterList")
    public fun roundedRect(
        x: Int,
        y: Int,
        width: Int,
        height: Int,
        xRad: Int,
        yRad: Int
    )

    @Suppress("LongParameterList")
    public fun textInRect(
        text: String,
        x: Int,
        y: Int,
        width: Int,
        height: Int,
        font: Font
    )
    public fun oval(x: Int, y: Int, width: Int, height: Int)
    public fun line(x1: Int, y1: Int, x2: Int, y2: Int)
    public fun polygon(points: Iterable<Point>)
    public fun renderSvg(svg: SvgElement, rectangle: Rectangle, scaleMode: ScaleMode)
}
