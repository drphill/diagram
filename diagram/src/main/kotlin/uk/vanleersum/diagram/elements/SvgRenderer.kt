package uk.vanleersum.diagram.elements
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import org.apache.logging.log4j.kotlin.Logging
import uk.vanleersum.ksvg.svg.SvgElement
import java.awt.Color
import java.awt.Font
import java.awt.Point
import java.awt.Rectangle
import java.io.Writer
@Suppress("MagicNumber", "TooManyFunctions")
public class SvgRenderer(
    private val writer: Writer,
    width: Int,
    height: Int,
    backgroundColour: Color = Color.WHITE
) : Logging, Renderer {
    override var lineDash: Iterable<Int>? = null
    override var strokeColour: Color? = null
    override var strokeWidth: Float = 1f
    private val strokeString: String get() = colourString(strokeColour)
    override var fillColour: Color? = null
    override var italic: Boolean = false

    init {
        val c = colourString(backgroundColour)
        writer.write("""<?xml version="1.0" encoding="UTF-8" standalone="no"?>""")
        writer.write("\n")
        writer.write("""<svg width="$width" height="$height" xmlns="http://www.w3.org/2000/svg">""")
        writer.write("\n")
        writer.write("""<rect fill="$c" stroke="#000" width="$width" height="$height"/>""")
        writer.write("\n")
    }

    override fun close() {
        writer.write("""</svg>""")
        writer.write("\n")
    }

    override fun comment(string: String) {
        writer.write("\n<!--<![CDATA[$string]]>-->\n")
    }

//    override fun stroke(colour: Color?) {
//        strokeString = colourString(colour)
//    }

    private fun fillString(): String {
        val c = fillColour
        return if (null == c) {
            """ fill-opacity="0" """
        } else {
            val v = String.format(null, "#%06x", 0xFFFFFF and c.rgb)
            """ fill="$v" """
        }
    }

    private fun colourString(colour: Color?) = if (null == colour) {
        "#ffffff"
    } else {
        String.format(null, "#%06x", 0xFFFFFF and colour.rgb)
    }

    override fun rect(x: Int, y: Int, width: Int, height: Int) {
        writer.write(
            """
            <rect x="$x" y="$y" width="$width" height="$height" ${fillString()} 
                stroke-width="$strokeWidth" stroke="$strokeString"/>
            """.trimIndent()
        )
        writer.write("\n")
    }

    @Suppress("LongParameterList")
    override fun roundedRect(
        x: Int,
        y: Int,
        width: Int,
        height: Int,
        xRad: Int,
        yRad: Int
    ) {
        writer.write(
            """
            <rect x="$x" y="$y" width="$width" height="$height"  
                  rx="$xRad" ry="$yRad" ${fillString()} 
                  stroke-width="$strokeWidth" stroke="$strokeString"/>
            """.trimIndent()
        )
        writer.write("\n")
    }

    @Suppress("LongParameterList")
    override fun textInRect(
        text: String,
        x: Int,
        y: Int,
        width: Int,
        height: Int,
        font: Font
    ) {
        val style = if (italic) {
            "italic"
        } else {
            "normal"
        }

        val weight = if (font.isBold) {
            "bold"
        } else {
            "normal"
        }

        writer.write(
            """
             <text x="$x" y="${ y + font.size}" font-size="${font.size}" 
                   font-style="$style" font-weight="$weight" ${fillString()}
                  >
                $text
            </text>
            """.trimIndent()
        )
        writer.write("\n")
    }

    override fun oval(x: Int, y: Int, width: Int, height: Int) {
        writer.write(
            """
            <ellipse cx="$x" cy="$y"  
                  rx="${width / 2}" ry="${height / 2}" ${fillString()} 
                  stroke-width="$strokeWidth" stroke="$strokeString"/>
            """.trimIndent()
        )
    }

    override fun line(x1: Int, y1: Int, x2: Int, y2: Int) {
        writer.write(
            """<line stroke="$strokeString" $lineDashString 
            |                 x1="$x1" x2="$x2" y1="$y1" y2="$y2" />""".trimMargin()
        )
        writer.write("\n")
    }

    override fun polygon(points: Iterable<Point>) {
        val pointString = points.joinToString(",") { "${it.x},${it.y}" }
        val str = """<polygon ${fillString()} stroke="$strokeString" 
            |   stroke-width="$strokeWidth" points="$pointString" />""".trimMargin()
        writer.write(str)
        writer.write("\n")
    }

    override fun renderSvg(svg: SvgElement, rectangle: Rectangle, scaleMode: ScaleMode) {
        logger.debug { "SVG render to file not implemented yet" }
    }

    private val lineDashString: String get() {
        lineDash?.let {
            val s = it.joinToString(",") { it.toString() }
            return """ stroke-dasharray="$s" """
        }
        return ""
    }
}
