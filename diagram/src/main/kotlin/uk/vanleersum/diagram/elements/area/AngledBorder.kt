package uk.vanleersum.diagram.elements.area

import uk.vanleersum.diagram.editor.DiagramEditor
import uk.vanleersum.diagram.editor.highlightColour
import uk.vanleersum.diagram.editor.selected
import uk.vanleersum.diagram.elements.Renderer
import uk.vanleersum.diagram.elements.geometry.Line
import java.awt.Point
import java.awt.Polygon
import java.awt.font.FontRenderContext

@Suppress("MagicNumber")
public class AngledBorder(public override var element: AreaElement) : Border {
    private val intStroke get() = 0

    override var style: Border.Style = Border.MutableStyle()

    public override val max: Point
        get() = Point(x + width, y + height)

    override var orthogonalIntersect: Boolean = false
    override var x: Int = 0
        set(value) {
            field = value
            element.x = x + intStroke + height / 2
        }
    override var y: Int = 0
        set(value) {
            field = value
            element.y = y + intStroke
        }

    override fun childAt(point: Point): AreaElement? {
        return element.childAt(point)
    }

    override var width: Int = 0
        private set
    override var height: Int = 0
        private set

    private var specifiedCenter: Point? = null
    override var centre: Point
        get() = specifiedCenter ?: super.centre
        set(value) {
            specifiedCenter = value
            super.centre = value
        }

    override fun move(dx: Int, dy: Int) {
        specifiedCenter?.let {
            it.translate(dx, dy)
            super.centre = it
            return
        }
        super.move(dx, dy)
    }

    override fun calculate(context: FontRenderContext) {
        element.calculate(context)
        height = element.height + intStroke + intStroke
        width = element.width + intStroke + intStroke + height
        specifiedCenter?.let {
            x = it.x - width / 2
            y = it.y - height / 2
        }
        element.x = x + intStroke + height / 2
        element.y = y + intStroke
    }

    private val inset get() = height / 2
    private val polygon get() = Polygon(xPts, yPts, xPts.size)
    private val xPts get() = intArrayOf(x, x + inset, x + width - inset, x + width, x + width - inset, x + inset)
    private val yPts get() = intArrayOf(centre.y, y, y, centre.y, y + height, y + height)

    private val points get() = listOf(
        Point(x, centre.y),
        Point(x + inset, y),
        Point(x + width - inset, y),
        Point(x + width, centre.y),
        Point(x + width - inset, y + height),
        Point(x + inset, y + height)
    )

    override fun intersectFrom(point: Point): Point {
        return if (orthogonalIntersect) {
            val midY = y + height / 2
            val lhx = x + inset
            val rhx = x + width - inset
            when {
                point.x < lhx -> {
                    val incoming = Line(point, Point(lhx, midY))
                    listOf(
                        Line(x, midY, lhx, y),
                        Line(x, midY, lhx, y + height)
                    ).mapNotNull { incoming.boundedIntersection(it) }
                        .firstOrNull()
                        ?: centre
                }
                point.x > rhx -> {
                    val incoming = Line(point, Point(rhx, midY))
                    listOf(
                        Line(x + width, midY, rhx, y),
                        Line(x + width, midY, rhx, y + height)
                    ).mapNotNull { incoming.boundedIntersection(it) }
                        .firstOrNull()
                        ?: centre
                }
                point.y < midY -> Point(point.x, y)
                else -> Point(point.x, y + height)
            }
        } else {
            val radial = Line(centre, point)
            val points = this.points
            points
                .plus(points.first())
                .zipWithNext()
                .map { Line(it.first, it.second) }
                .mapNotNull { radial.boundedIntersection(it) }
                .minByOrNull { it.distance(point) }
                ?: centre
        }
    }

    override fun contains(point: Point): Boolean {
        return polygon.contains(point)
    }

    override fun render(renderer: Renderer) {
        if (selected) {
            DiagramEditor.glowColour.forEachIndexed { index, color ->
                renderer.strokeWidth = index.toFloat()
                renderer.strokeColour = color
                renderer.polygon(points)
            }
        } else if (null != highlightColour) {
            highlightColour?.forEachIndexed { index, color ->
                renderer.strokeWidth = index.toFloat()
                renderer.strokeColour = color
                renderer.polygon(points)
            }
        }

        renderer.stroke(style.borderColour)
        renderer.fillColour = style.backgroundColour
        renderer.strokeWidth = style.strokeWidth

        val points = listOf(
            Point(x, centre.y),
            Point(x + inset, y),
            Point(x + width - inset, y),
            Point(x + width, centre.y),
            Point(x + width - inset, y + height),
            Point(x + inset, y + height)
        )
        renderer.polygon(points)
        element.render(renderer)
    }
}
