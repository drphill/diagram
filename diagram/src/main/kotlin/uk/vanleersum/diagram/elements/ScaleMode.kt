package uk.vanleersum.diagram.elements

/**
 * Indicates how to fit content into an area
 */
public enum class ScaleMode {
    /**
     * Render at the specified size
     */
    None,

    /**
     * Scale to fit the width, the aspect ratio is maintained
     */
    FitWidth,

    /**
     * Scale to fit the height, the aspect ratio is maintained
     */
    FitHeight,

    /**
     * Scale to fit the maximum size possible without exceeding the area bounds,
     * the aspect ratio is maintained
     */
    AspectFit,

    /**
     * Scale to fit so that the entire area is covered, some of the content may fall
     * outside the area, the aspect ratio is maintained
     */
    Fill
}
