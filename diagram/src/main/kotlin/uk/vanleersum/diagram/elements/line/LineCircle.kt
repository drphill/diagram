/*******************************************************************************
 * Copyright (C) 2017 Dr Phill van Leersum
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http:></http:>//www.gnu.org/licenses/>.
 */
package uk.vanleersum.diagram.elements.line

import uk.vanleersum.diagram.elements.Renderer
import java.awt.Point
import java.awt.font.FontRenderContext

/**
 * Add a circle to a line such that the arrow automatically adjusts its position when the line changes
 * @author phill
 */
public class LineCircle(public val radius: Int = 10) : LineDecoration {
    public var centreX: Int = 0
    public var centreY: Int = 0

    override fun calculate(src: Point, dst: Point) {
        val srcX: Double = src.getX()
        val dstX: Double = dst.getX()
        val srcY: Double = src.getY()
        val dstY: Double = dst.getY()
        val deltaX = srcX - dstX
        val deltaY = srcY - dstY
        // calculate length of line between points
        val lineLength = Math.sqrt(deltaX * deltaX + deltaY * deltaY)
        // what proportion of this do we want?
        val shapeLengthLineLengthRatio: Double = this.radius / lineLength // small arrow length is 20?
        val tipX = dstX + shapeLengthLineLengthRatio * deltaX
        val tipY = dstY + shapeLengthLineLengthRatio * deltaY
        centreX = tipX.toInt()
        centreY = tipY.toInt()
    }

    override fun calculate(context: FontRenderContext) {
        // irrelevant
    }

    override fun render(renderer: Renderer) {
        renderer.strokeWidth = 1f
        renderer.oval(centreX - radius, centreY - radius, radius + radius, radius + radius)
    }
}
