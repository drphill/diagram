package uk.vanleersum.diagram.elements.geometry

import java.awt.Point
import java.awt.geom.Line2D
import java.lang.Integer.max
import java.lang.Integer.min
import java.util.*

public class Line(public val start: Point, public val end: Point) {
    public constructor(startX: Int, startY: Int, endX: Int, endY: Int) : this(Point(startX, startY), Point(endX, endY))

    public fun distance(point: Point): Double {
        return Line2D.ptLineDist(
            start.x.toDouble(), start.y.toDouble(),
            end.x.toDouble(), end.y.toDouble(),
            point.x.toDouble(), point.y.toDouble()
        )
    }

    /**
     * Return the point at which the projection of the receiver intersects the
     * projection of the given line
     */
    public fun intersection(line: Line): Point? {
        val a1 = end.y - start.y
        val b1 = start.x - end.x
        val c1 = a1 * start.x + b1 * start.y
        val a2 = line.end.y - line.start.y
        val b2 = line.start.x - line.end.x
        val c2 = a2 * line.start.x + b2 * line.start.y
        val delta = a1 * b2 - a2 * b1
        if (delta == 0) {
            return null
        }
        return Point((b2 * c1 - b1 * c2) / delta, (a1 * c2 - a2 * c1) / delta)
    }

    /**
     * return the intersection point of the receiver and the line only if the point
     * lies within the defined segments.
     */
    public fun boundedIntersection(line: Line): Point? {
        // since we know that the point is on the line we can do simple range checks
        intersection(line)?.let { point ->
            if (isWithinBounds(point) && line.isWithinBounds(point)) {
                return point
            }
        }
        return null
    }

    /**
     * return true if the point is within the logical bounds of the
     * receiver.  The logical bounds are a rectangle defined by the
     * start and end points of the receiver (both inclusive).
     */
    public fun isWithinBounds(point: Point): Boolean {
        return (minX..maxX).contains(point.x) &&
            (minY..maxY).contains(point.y)
    }

    /**
     * return true if the point is within the logical bounds of the
     * receiver.  The logical bounds are a rectangle defined by the
     * start and end points of the receiver (both inclusive).
     */
    public fun isWithinBounds(point: Point, accuracy: Int): Boolean {
        return ((minX - accuracy)..(maxX + accuracy)).contains(point.x) &&
            ((minY - accuracy)..(maxY + accuracy)).contains(point.y)
    }

    public val maxX: Int get() = max(start.x, end.x)
    public val minX: Int get() = min(start.x, end.x)
    public val maxY: Int get() = max(start.y, end.y)
    public val minY: Int get() = min(start.y, end.y)

    @Suppress("ReturnCount")
    // https://stackoverflow.com/questions/13053061/circle-line-intersection-points
    internal fun getCircleIntersectionPoints(center: Point, radius: Double): List<Point> {
        val baX = (end.x - start.x).toDouble()
        val baY = (end.y - start.y).toDouble()
        val caX = (center.x - start.x).toDouble()
        val caY = (center.y - start.y).toDouble()
        val a = baX * baX + baY * baY
        val bBy2 = baX * caX + baY * caY
        val c = caX * caX + caY * caY - radius * radius
        val pBy2 = bBy2 / a
        val q = c / a
        val disc = pBy2 * pBy2 - q
        if (disc < 0) {
            return emptyList()
        }
        // if disc == 0 ... dealt with later
        val tmpSqrt = Math.sqrt(disc)
        val abScalingFactor1 = -pBy2 + tmpSqrt
        val abScalingFactor2 = -pBy2 - tmpSqrt
        val p1 = Point(
            (start.x - baX * abScalingFactor1).toInt(),
            (
                start.y -
                    baY * abScalingFactor1
                ).toInt()
        )
        if (disc == 0.0) { // abScalingFactor1 == abScalingFactor2
            return listOf(p1)
        }
        val p2 = Point(
            (start.x - baX * abScalingFactor2).toInt(),
            (
                start.y -
                    baY * abScalingFactor2
                ).toInt()
        )
        return Arrays.asList(p1, p2)
    }

    internal fun getBoundedCircleIntersectionPoints(center: Point, radius: Double): List<Point> {
        return getCircleIntersectionPoints(center, radius)
            .filter { isWithinBounds(it) }
    }
}
