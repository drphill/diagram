package uk.vanleersum.diagram.elements.area
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import org.apache.logging.log4j.kotlin.Logging
import uk.vanleersum.diagram.editor.DiagramEditor
import uk.vanleersum.diagram.editor.highlightColour
import uk.vanleersum.diagram.editor.selected
import uk.vanleersum.diagram.elements.Renderer
import uk.vanleersum.diagram.elements.geometry.Line
import uk.vanleersum.diagram.elements.geometry.intersectionOfLineToCenter
import uk.vanleersum.diagram.elements.geometry.intersectionOfOrthogonalLineToCenter
import java.awt.Point
import java.awt.font.FontRenderContext

@Suppress("MagicNumber")
public class RoundedBorder(
    public override var element: AreaElement,
    override var style: Border.Style = Border.MutableStyle()
) : Logging, Border {
    override var orthogonalIntersect: Boolean = false

    override var x: Int = 0
        set(value) {
            field = value
            element.x = x + 0 + element.height / 2
        }

    override var y: Int = 0
        set(value) {
            field = value
            element.y = y + 0
        }
    override var width: Int = 0
        private set
    override var height: Int = 0
        private set

    private val arcsize get() = height

    override fun childAt(point: Point): AreaElement? {
        return element.childAt(point)
    }

    private var specifiedCenter: Point? = null
    override var centre: Point
        get() = specifiedCenter ?: super.centre
        set(value) {
            specifiedCenter = value
            super.centre = value
        }

    override fun move(dx: Int, dy: Int) {
        specifiedCenter?.let {
            it.translate(dx, dy)
            super.centre = it
            return
        }
        super.move(dx, dy)
    }

    override fun calculate(context: FontRenderContext) {
        element.calculate(context)
        val extra = element.height
        width = element.width + extra
        height = element.height
        specifiedCenter?.let {
            x = it.x - width / 2
            y = it.y - height / 2
        }
        element.x = x + (extra / 2)
        element.y = y
    }

    override fun render(renderer: Renderer) {
        if (selected) {
            DiagramEditor.glowColour.forEachIndexed { index, color ->
                renderer.strokeWidth = index.toFloat()
                renderer.strokeColour = color
                renderer.roundedRect(x, y, width, height, arcsize / 2, arcsize / 2)
            }
        } else if (null != highlightColour) {
            highlightColour?.forEachIndexed { index, color ->
                renderer.strokeWidth = index.toFloat()
                renderer.strokeColour = color
                renderer.roundedRect(x, y, width, height, arcsize / 2, arcsize / 2)
            }
        }
        renderer.fillColour = style.backgroundColour
        renderer.stroke(style.borderColour)
        renderer.strokeWidth = style.strokeWidth
        renderer.roundedRect(x, y, width, height, arcsize / 2, arcsize / 2)
        element.render(renderer)
    }

    override fun intersectFrom(point: Point): Point {
        val radius = height / 2
        val rh = Point(max.x - radius, y + height / 2)
        val lh = Point(x + radius, y + height / 2)

        return if (orthogonalIntersect) {
            val i = bounds.intersectionOfOrthogonalLineToCenter(point)

            if (i.x > rh.x) {
                Line(rh, point).getCircleIntersectionPoints(rh, radius.toDouble())
                    .minByOrNull { point.distance(it) } ?: centre
            } else if (i.x < lh.x) {
                Line(lh, point).getCircleIntersectionPoints(lh, radius.toDouble())
                    .minByOrNull { point.distance(it) } ?: centre
            } else {
                i
            }
        } else {
            val i = bounds.intersectionOfLineToCenter(point)
            when {
                i.x > rh.x -> Line(centre, point).getCircleIntersectionPoints(rh, radius.toDouble())
                    .minByOrNull { point.distance(it) } ?: centre
                i.x < lh.x -> Line(centre, point).getCircleIntersectionPoints(lh, radius.toDouble())
                    .minByOrNull { point.distance(it) } ?: centre
                else -> i
            }
        }
    }
}
