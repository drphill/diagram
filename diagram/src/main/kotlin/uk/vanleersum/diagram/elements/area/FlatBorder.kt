package uk.vanleersum.diagram.elements.area
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import uk.vanleersum.diagram.editor.DiagramEditor
import uk.vanleersum.diagram.editor.highlightColour
import uk.vanleersum.diagram.editor.selected
import uk.vanleersum.diagram.elements.Renderer
import uk.vanleersum.diagram.elements.geometry.intersectionOfLineToCenter
import uk.vanleersum.diagram.elements.geometry.intersectionOfOrthogonalLineToCenter
import java.awt.Point
import java.awt.font.FontRenderContext

@Suppress("MagicNumber")
public class FlatBorder(
    public override var element: AreaElement,
    override var style: Border.Style = Border.MutableStyle()
) : Border {
    override var orthogonalIntersect: Boolean = false

    override var x: Int = 0
        set(value) {
            field = value
            element.x = x
        }

    override var y: Int = 0
        set(value) {
            field = value
            element.y = y
        }

    override var width: Int = 0
        private set

    override var height: Int = 0
        private set

    override fun childAt(point: Point): AreaElement? {
        return element.childAt(point)
    }

    private var specifiedCenter: Point? = null
    override var centre: Point
        get() = specifiedCenter ?: super.centre
        set(value) {
            specifiedCenter = value
            super.centre = value
        }

    override fun move(dx: Int, dy: Int) {
        specifiedCenter?.let {
            it.translate(dx, dy)
            super.centre = it
            return
        }
        super.move(dx, dy)
    }

    override fun calculate(context: FontRenderContext) {
        element.calculate(context)
        width = element.width
        height = element.height
        specifiedCenter?.let {
            x = it.x - width / 2
            y = it.y - height / 2
        }
        element.x = x
        element.y = y
    }

    override fun render(renderer: Renderer) {
        renderer.lineDash = null
        if (selected) {
            DiagramEditor.glowColour.forEachIndexed { index, color ->
                renderer.strokeWidth = index.toFloat()
                renderer.strokeColour = color
                renderer.rect(x, y, width, height)
            }
        } else if (null != highlightColour) {
            highlightColour?.forEachIndexed { index, color ->
                renderer.strokeWidth = index.toFloat()
                renderer.strokeColour = color
                renderer.rect(x, y, width, height)
            }
        }
        renderer.fillColour = style.backgroundColour
        renderer.stroke(style.borderColour)
        renderer.strokeWidth = style.strokeWidth
        renderer.rect(x, y, width, height)
        element.render(renderer)
    }

    override fun intersectFrom(point: Point): Point = if (orthogonalIntersect) {
        bounds.intersectionOfOrthogonalLineToCenter(point)
    } else {
        bounds.intersectionOfLineToCenter(point)
    }
}
