package uk.vanleersum.diagram.elements.area
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import uk.vanleersum.diagram.elements.Renderer
import uk.vanleersum.diagram.elements.geometry.intersectionOfLineToCenter
import uk.vanleersum.diagram.elements.geometry.intersectionOfOrthogonalLineToCenter
import java.awt.Color
import java.awt.Point
import java.awt.font.FontRenderContext

/**
 * Area of colour surrounding an [AreaElement]
 */
@Suppress("MagicNumber")
public class RectangularPadding(
    public var element: AreaElement,
    public var top: Int = 5,
    public var left: Int = top,
    public var bottom: Int = top,
    public var right: Int = left,
) : AreaElement {
    public var backgroundColour: Color? = null
    override var orthogonalIntersect: Boolean = true

    override var x: Int = 0
        set(value) {
            field = value
            element.x = x + left
        }

    public fun reset(top: Int = 5, left: Int = top, bottom: Int = top, right: Int = left) {
        this.top = top
        this.left = left
        this.bottom = bottom
        this.right = right
    }

    override fun childAt(point: Point): AreaElement? {
        return element.childAt(point)
    }

    override var y: Int = 0
        set(value) {
            field = value
            element.y = y + top
        }
    override var width: Int = 0
        private set
    override var height: Int = 0
        private set

    override fun calculate(context: FontRenderContext) {
        element.calculate(context)
        width = element.width + left + right
        height = element.height + top + bottom
        element.x = x + left
        element.y = y + top
    }

    override fun render(renderer: Renderer) {
        renderer.stroke(null)
        renderer.strokeWidth = 0f
        renderer.fillColour = backgroundColour
        renderer.rect(x, y, width, height)
        element.render(renderer)
    }

    override fun intersectFrom(point: Point): Point {
        if (orthogonalIntersect) {
            return bounds.intersectionOfOrthogonalLineToCenter(point)
        } else {
            return bounds.intersectionOfLineToCenter(point)
        }
    }
}
