package uk.vanleersum.diagram.elements.area
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import uk.vanleersum.diagram.elements.Renderer
import uk.vanleersum.diagram.elements.geometry.intersectionOfLineToCenter
import uk.vanleersum.diagram.elements.geometry.intersectionOfOrthogonalLineToCenter
import java.awt.Point
import java.awt.font.FontRenderContext

public class VBox(elements: Iterable<AreaElement>) : AreaElement {
    public constructor(vararg elements: AreaElement) : this(elements.toList())
    public var elements: MutableList<AreaElement> = elements.toMutableList()
    public var topMargin: Int = 1
    public var leftMargin: Int = topMargin
    public var bottomMargin: Int = leftMargin
    public var rightMargin: Int = bottomMargin
    override var orthogonalIntersect: Boolean = true

    override fun intersectFrom(point: Point): Point = if (orthogonalIntersect) {
        bounds.intersectionOfOrthogonalLineToCenter(point)
    } else {
        bounds.intersectionOfLineToCenter(point)
    }

    override fun childAt(point: Point): AreaElement? {
        return elements.mapNotNull { it.childAt(point) }.firstOrNull()
    }

    override fun calculate(context: FontRenderContext) {
        elements.forEach { it.calculate(context) }
        width = leftMargin + rightMargin + (elements.maxOfOrNull { it.width } ?: 0)
        height = topMargin + bottomMargin + elements.sumOf { it.height }
        var yy = y + topMargin
        elements.forEach {
            it.x = x + (width - it.width) / 2
            it.y = yy
            yy += it.height
        }
    }

    override var width: Int = 0
        private set

    override var height: Int = 0
        private set

    override var x: Int = 0
        set(value) {
            field = value
            elements.forEach {
                it.x = x + (width - it.width) / 2
            }
        }

    override var y: Int = 0
        set(value) {
            field = value
            var yy = y + topMargin
            elements.forEach {
                it.y = yy
                yy += it.height
            }
        }

    override fun render(renderer: Renderer) {
        elements.forEach { it.render(renderer) }
    }
}
