package uk.vanleersum.diagram.elements
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import org.apache.logging.log4j.kotlin.Logging
import java.awt.Dimension
import java.awt.Font
import java.awt.FontFormatException
import java.awt.Toolkit
import java.util.*
import javax.swing.JLabel
import javax.swing.UIManager
import javax.swing.plaf.FontUIResource
public object FontManager : Logging {
    public const val LinesPerScreen: Int = 50
    public const val TitleMultiplier: Float = 2f
    public const val SubtitleMultiplier: Float = 1.5f
    public const val SmallMultiplier: Float = 0.75f

    public lateinit var defaultFont: Font
        private set
    public lateinit var italicFont: Font
        private set

    public lateinit var titleFont: Font
        private set
    public lateinit var subtitleFont: Font
        private set
    public lateinit var smallFont: Font
        private set
    public lateinit var smallItalic: Font
        private set

    init {
        setfonts(LinesPerScreen)
    }

    private fun setfonts(linesPerScreen: Int) {
        var font = JLabel().font
        var italic = JLabel().font

        @Suppress("TooGenericExceptionCaught")
        try {
            FontManager::class.java
                .getResourceAsStream("/fonts/Dinot Font.otf")?.use {
                    font = Font.createFont(Font.TRUETYPE_FONT, it)
                }
            FontManager::class.java
                .getResourceAsStream("/fonts/DINOT-Italic.ttf")?.use {
                    italic = Font.createFont(Font.TRUETYPE_FONT, it)
                }
        } catch (e: FontFormatException) {
            logger.error { "unable to load dinot $e" }
        } catch (e: NullPointerException) {
            logger.error { "unable to load dinot $e" }
        }
        val size: Dimension = Toolkit.getDefaultToolkit().getScreenSize()
        val fh = size.height / linesPerScreen
        defaultFont = font.deriveFont(fh.toFloat())
        italicFont = italic.deriveFont(fh.toFloat())

        titleFont = scaledFont(TitleMultiplier)
        subtitleFont = scaledFont(SubtitleMultiplier)
        smallFont = scaledFont(SmallMultiplier)
        smallItalic = italicFont.deriveFont(defaultFont.size * SmallMultiplier)
    }

    public fun setUIFont(linesPerScreen: Int = LinesPerScreen) {
        setfonts(linesPerScreen)
        setUIFont(defaultFont)
    }

    public fun scaledFont(scale: Float): Font = defaultFont.deriveFont(defaultFont.size * scale)

    public fun setUIFont(font: Font?) {
        val uir = FontUIResource(font)

        val keys: Enumeration<*> = UIManager.getDefaults().keys()
        while (keys.hasMoreElements()) {
            val key = keys.nextElement()
            val value = UIManager.get(key)
            if (value is FontUIResource) UIManager.put(key, uir)
        }
    }
}
