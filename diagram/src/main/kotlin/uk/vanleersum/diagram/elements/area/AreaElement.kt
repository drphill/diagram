package uk.vanleersum.diagram.elements.area
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import uk.vanleersum.diagram.elements.Element
import java.awt.Point
import java.awt.Rectangle

public interface AreaElement : Element {

    override fun contains(point: Point): Boolean = bounds.contains(point)
    override val max: Point get() = Point(x + width, y + height)
    override fun isContainedWithin(rectangle: Rectangle): Boolean {
        return rectangle.contains(bounds)
    }

    public val bounds: Rectangle get() = Rectangle(x, y, width, height)
    public var centre: Point
        get() = Point(x + width / 2, y + height / 2)
        set(value) {
            x = value.x - width / 2
            y = value.y - height / 2
        }
    public fun intersectFrom(point: Point): Point = centre
    public var orthogonalIntersect: Boolean

    override fun move(dx: Int, dy: Int) {
        x += dx
        y += dy
    }

    public val width: Int
    public val height: Int
    public var x: Int
    public var y: Int

    public fun childAt(point: Point): AreaElement?
}
