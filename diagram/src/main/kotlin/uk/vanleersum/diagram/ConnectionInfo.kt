package uk.vanleersum.diagram
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */
import uk.vanleersum.diagram.editor.DiagramEditor
import uk.vanleersum.diagram.elements.area.AreaElement
import java.awt.Color
import java.awt.Point

/**
 * Description of the connection line that is required when dragging from an [AreaElement].
 * This allows the source of the line to control the colour of the line according to where
 * it is being dragged, and allows the source of the line to perform the appropriate
 * action when the line is dropped.
 */
public interface ConnectionInfo {
    public companion object {
        public val defaultAcceptColour: Color = Color.GREEN
        public val defaultDeclineColour: Color = Color.RED
        public val defaultAcceptHighlightColour: Iterable<Color> = DiagramEditor.acceptHighlightColour
        public val defaultDeclineHighlightColour: Iterable<Color> = DiagramEditor.declineHighlightColour
    }
    public val sourceElement: AreaElement
    public val sinkElement: AreaElement?
    public val connectionColour: Color get() = Color.RED
    public val connectionStrokeWidth: Float get() = 2f
    public val connectionLineDashArray: FloatArray? get() = null
    public val highlightColour: Iterable<Color>? get() = defaultAcceptHighlightColour

    /**
     * [source] and [sink] are the root [AreaElement]s. The receiver is responsible for determining
     * the nested element by using the supplied coordinates
     */
    public fun dragging(source: AreaElement, sourcePoint: Point, sink: AreaElement?, sinkPoint: Point)
    /**
     * [source] and [sink] are the root [AreaElement]s. The receiver is responsible for determining
     * the nested element by using the supplied coordinates
     */
    public fun dropped(source: AreaElement, sourcePoint: Point, sink: AreaElement?, sinkPoint: Point)
}
