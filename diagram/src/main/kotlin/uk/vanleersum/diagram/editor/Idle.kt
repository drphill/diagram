package uk.vanleersum.diagram.editor

import uk.vanleersum.diagram.ConnectionInfo
import uk.vanleersum.diagram.elements.area.AreaElement
import uk.vanleersum.diagram.elements.line.LineElement
import java.awt.event.MouseEvent

internal class Idle(private val editor: DiagramEditor) : EditorState {
    init {
        // editor.logger.debug { "Idle" }
        HighlightState.clear()
    }

    override fun mouseClicked(e: MouseEvent): EditorState {
        if (e.clickCount > 1) {
            editor.doubleClick(e.point)
        }
        return this
    }

    override fun mousePressed(e: MouseEvent): EditorState {
        var newState: EditorState = this
        if (e.isPopupTrigger) {
            // editor.logger.debug { "popup trigger" }
            editor.popup(e.point)
        } else {
            val element = editor[e.point]
            newState = when {
                null == element -> MouseDownOnBackground(editor, e.point)
                element.selected -> MouseDownOnSelectedElement(editor, e.point, element)
                element is AreaElement -> MouseDownOnAreaElement(editor, e.point, element)
                element is LineElement -> MouseDownOnLineElement(editor, element)
                else -> this
            }
        }
        return newState
    }

    override fun mouseMoved(e: MouseEvent): EditorState {
        val areaElement = editor[e.point] as? AreaElement
        if (null == areaElement) {
            HighlightState.clear()
        } else if (!areaElement.selected) {
            editor.connectionInfo(areaElement, e.point)?.let {
                it.sourceElement.highlightColour = ConnectionInfo.defaultAcceptHighlightColour
                editor.redraw()
                return OverConnectableAreaElement(editor, areaElement, it.sourceElement)
            }
        }
        return this
    }
}
