package uk.vanleersum.diagram.editor

import java.awt.event.MouseEvent

internal sealed interface EditorState {
    fun mouseClicked(e: MouseEvent): EditorState = this
    fun mousePressed(e: MouseEvent): EditorState = this
    fun mouseReleased(e: MouseEvent): EditorState = this
    fun mouseEntered(e: MouseEvent): EditorState = this
    fun mouseExited(e: MouseEvent): EditorState = this
    fun mouseDragged(e: MouseEvent): EditorState = this
    fun mouseMoved(e: MouseEvent): EditorState = this
}
