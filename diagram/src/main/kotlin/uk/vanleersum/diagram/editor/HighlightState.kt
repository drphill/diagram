package uk.vanleersum.diagram.editor

import uk.vanleersum.diagram.elements.Element
import java.awt.Color
import java.util.*

internal object HighlightState {
    private val highlight = WeakHashMap<Element, Iterable<Color>>()

    internal operator fun get(element: Element): Iterable<Color>? {
        return highlight[element]
    }

    internal operator fun set(element: Element, colour: Iterable<Color>?) {
        if (null != colour) {
            highlight.put(element, colour)
        } else {
            highlight.remove(element)
        }
    }

    internal fun clear() {
        highlight.clear()
    }
}

internal var Element.highlightColour: Iterable<Color>?
    get() = HighlightState[this]
    set(value) {
        HighlightState[this] = value
    }

// internal var Element.highlight: Boolean
//    get() = highlightColour != null
//    set(value) {
//        highlightColour = if (value) {
//            highlightColour ?: DiagramEditor.acceptHighlightColour
//        } else {
//            null
//        }
//    }
