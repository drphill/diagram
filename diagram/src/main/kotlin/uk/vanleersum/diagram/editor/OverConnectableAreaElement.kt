package uk.vanleersum.diagram.editor
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import uk.vanleersum.diagram.ConnectionInfo
import uk.vanleersum.diagram.elements.area.AreaElement
import java.awt.event.MouseEvent

internal class OverConnectableAreaElement(
    private val editor: DiagramEditor,
    private val rootElement: AreaElement,
    private val connectableElement: AreaElement
) : EditorState {
    // init { editor.logger.debug {"OverConnectableAreaElement"}}

    override fun mouseMoved(e: MouseEvent): EditorState {
        val overRootElement = editor[e.point] as? AreaElement
        if (null == overRootElement) {
            connectableElement.highlightColour = null
            editor.redraw()
        } else {

            val connectionInfo = editor.connectionInfo(overRootElement, e.point)

            val highlightElement = connectionInfo?.sourceElement
            if (connectableElement != highlightElement) {
                connectableElement.highlightColour = null
                connectionInfo?.sourceElement?.highlightColour = ConnectionInfo.defaultAcceptHighlightColour
                connectionInfo?.sinkElement?.highlightColour = connectionInfo?.highlightColour
                editor.redraw()
            }
            if (null != highlightElement) {
                return OverConnectableAreaElement(editor, overRootElement, highlightElement)
            }
        }
        return Idle(editor)
    }

    override fun mousePressed(e: MouseEvent): EditorState {
        connectableElement.highlightColour = null
        return if (e.isPopupTrigger) {
            // editor.logger.debug { "popup trigger" }
            editor.redraw()
            editor.popup(e.point)
            this
        } else {
            editor.redraw()
            MouseDownOnAreaElement(editor, e.point, rootElement)
        }
    }
}
