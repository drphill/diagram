package uk.vanleersum.diagram.editor
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */

import uk.vanleersum.diagram.elements.line.LineElement
import java.awt.event.MouseEvent

internal class MouseDownOnLineElement(
    private val editor: DiagramEditor,
    private val element: LineElement
) : EditorState {
    init {
        // editor.logger.debug { "MouseDownOnLineElement" }
        editor.select(null, false)
    }

    override fun mouseClicked(e: MouseEvent): EditorState {
        editor.select(element, e.isControlDown)
        return Idle(editor)
    }

    override fun mouseReleased(e: MouseEvent): EditorState {
        editor.select(element, e.isControlDown)
        return Idle(editor)
    }

    override fun mouseDragged(e: MouseEvent): EditorState {
        element.labelHit(e.point)?.let {
            editor.redraw()
            return MouseDraggingLineLabel(editor, e.point, element, it)
        }
        val waypoint = element.dragStart(e.point)
        editor.redraw()
        return MouseDraggingLineWaypoint(editor, e.point, element, waypoint)
    }
}
