package uk.vanleersum.diagram.editor
/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */
import org.apache.logging.log4j.kotlin.Logging
import uk.vanleersum.diagram.ConnectionInfo
import uk.vanleersum.diagram.DiagramPanel
import uk.vanleersum.diagram.EditorDelegate
import uk.vanleersum.diagram.add
import uk.vanleersum.diagram.elements.Element
import uk.vanleersum.diagram.elements.Renderer
import uk.vanleersum.diagram.elements.area.AreaElement
import uk.vanleersum.diagram.elements.line.LineArrow
import uk.vanleersum.diagram.elements.line.LineElement
import java.awt.Color
import java.awt.Point
import java.awt.Rectangle
import java.awt.event.MouseEvent
import java.awt.event.MouseListener
import java.awt.event.MouseMotionListener
import javax.swing.JPopupMenu

@Suppress("TooManyFunctions")
internal class DiagramEditor internal constructor(private val diagramPanel: DiagramPanel) : Logging {
    private var selectionRectangle: Rectangle? = null
    private var connectionLine: LineElement? = null

    internal var editorDelegate: EditorDelegate? = null
        set(value) {
            field = value
            if (null == field) {
                disconnect()
            } else {
                connect()
            }
        }

    private var editorState: EditorState = Idle(this)

    private val mouseListener = object : MouseListener {
        override fun mouseClicked(e: MouseEvent) { editorState = editorState.mouseClicked(e) }
        override fun mousePressed(e: MouseEvent) { editorState = editorState.mousePressed(e) }
        override fun mouseReleased(e: MouseEvent) { editorState = editorState.mouseReleased(e) }
        override fun mouseEntered(e: MouseEvent) { editorState = editorState.mouseEntered(e) }
        override fun mouseExited(e: MouseEvent) { editorState = editorState.mouseExited(e) }
    }

    private val mouseMotionListener = object : MouseMotionListener {
        override fun mouseDragged(e: MouseEvent) {
            editorState = editorState.mouseDragged(e)
        }
        override fun mouseMoved(e: MouseEvent) {
            editorState = editorState.mouseMoved(e)
        }
    }

    internal fun connect() {
        // disconnect first in case we are called multiple times. Multiple
        // disconnects are a no-op
        disconnect()
        diagramPanel.addMouseMotionListener(mouseMotionListener)
        diagramPanel.addMouseListener(mouseListener)
    }

    internal fun disconnect() {
        diagramPanel.removeMouseMotionListener(mouseMotionListener)
        diagramPanel.removeMouseListener(mouseListener)
        deselectSelected()
        reset()
        redraw()
    }

    // this accepts a scaled point
    internal operator fun get(point: Point): Element? = diagramPanel.diagram[point]
    internal fun redraw() = diagramPanel.redraw()

    internal fun select(element: Element?, additive: Boolean) {
        selectionRectangle = null
        if (!additive) {
            deselectSelected()
        }
        element?.apply {
            selected = true
        }
        redraw()
    }

    private fun deselectSelected() {
        val selected = diagramPanel.diagram.filter { it.selected }
        if (selected.isNotEmpty()) {
            selected.forEach { it.selected = false }
        }
    }

    internal fun selectRectangle(rectangle: Rectangle?) {
        selectionRectangle = rectangle
        rectangle?.let { rect ->
            val selectThese = diagramPanel.diagram
                .filter { it.isContainedWithin(rect) }
            if (selectThese.isNotEmpty()) {
                selectThese.forEach { it.selected = true }
            }
        }
        redraw()
    }

    internal fun moveSelectedElements(dx: Int, dy: Int) {
        diagramPanel.diagram
            .filter { it.selected }
            .forEach { it.move(dx, dy) }
        redraw()
    }

    internal val selectedItemCount get() = diagramPanel.diagram.count { it.selected }

    internal fun calculate() {
        connectionLine?.calculate(diagramPanel.diagram.fontRenderContext)
    }

    internal fun render(renderer: Renderer) {
        selectionRectangle?.let {
            renderer.fillColour = selectionAreaColour
            renderer.strokeColour = null
            renderer.rect(it.x, it.y, it.width, it.height)
        }
        connectionLine?.render(renderer)
    }

    internal fun drawConnectionArrow(dst: Point, info: ConnectionInfo) {
        if (null == connectionLine) {
            connectionLine = LineElement().apply {
                endArrow = LineArrow()
            }
        }
        connectionLine?.apply {
            startElement = info.sourceElement
            end = dst
            style = LineElement.MutableStyle(
                info.connectionColour,
                info.connectionStrokeWidth,
                info.connectionLineDashArray
            )
        }
        redraw()
    }

    internal fun hideConnectionArrow() {
        connectionLine = null
        redraw()
    }

    internal fun connectionInfo(element: AreaElement, at: Point): ConnectionInfo? =
        editorDelegate?.connectionInfo(at, element)

    internal fun popup(point: Point) {
        val element = diagramPanel.diagram[point]
        (element as? LineElement)?.let {
            it.waypoint(point)?.let {
                JPopupMenu().apply {
                    add("Delete Waypoint") {
                        element.waypoints.remove(it)
                        redraw()
                    }
                }.show(diagramPanel, point.x, point.y)
                return
            }
        }
        editorDelegate?.popup(point, element)
            ?.apply {
                val pt = diagramPanel.unscaled(point)
                show(diagramPanel, pt.x, pt.y)
            }
    }

    internal fun doubleClick(point: Point) =
        editorDelegate?.doubleClick(point, diagramPanel.diagram[point])

    internal fun reset() { editorState = Idle(this) }

    internal companion object {
        internal const val selectionWidth = 20f
        internal val selectionAreaColour = Color(0, 100, 255, 20)
        internal val selectionColour = Color(200, 220, 255, 100)

        private const val glowIterate = 30
        internal val glowColour: Iterable<Color> =
            (1..glowIterate).map {
                Color(230 - it - it, 255 - it - it, 255, glowIterate - it)
            }

        private const val highlightIterate = 30
        internal val acceptHighlightColour: Iterable<Color> =
            (1..highlightIterate).map {
                Color(220 - it - it, 255, 230 - it - it, highlightIterate - it)
            }
        internal val declineHighlightColour: Iterable<Color> =
            (1..highlightIterate).map {
                Color(255, 220 - it - it, 230 - it - it, highlightIterate - it)
            }
    }
}
