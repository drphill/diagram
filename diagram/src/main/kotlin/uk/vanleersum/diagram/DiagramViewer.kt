/*
 *  MIT License
 *
 *  Copyright (c) 2022 Phill van Leersum
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all
 *  copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */
package uk.vanleersum.diagram

import org.apache.logging.log4j.kotlin.Logging
import uk.vanleersum.diagram.elements.FontManager
import java.awt.BorderLayout
import java.awt.event.ComponentAdapter
import java.awt.event.ComponentEvent
import javax.swing.BorderFactory
import javax.swing.JPanel
import javax.swing.JScrollPane
import javax.swing.JSlider
import javax.swing.JTextArea

/**
 * A wrapper for a [DiagramPanel] that adds scrolling and scaling
 */
@Suppress("MagicNumber")
public class DiagramViewer(diagram: Diagram) : JPanel(BorderLayout()), Logging {
    public val panel: DiagramPanel = DiagramPanel()

    /**
     * Setting a non-null value will allow editing the diagram.  Setting a non-null
     * value will disallow editing the diagram.  The default is null.
     */
    public var editorDelegate: EditorDelegate?
        get() = panel.editorDelegate
        set(value) {
            panel.editorDelegate = value
        }

    private val scrollPane = JScrollPane(panel).apply {
        addComponentListener(object : ComponentAdapter() {
            override fun componentResized(e: ComponentEvent?) {
                resized()
            }
            override fun componentShown(e: ComponentEvent?) {
                resized()
            }
        })
    }

    private val scaleSlider = JSlider(1, 200, 100).apply {
        // orientation = JSlider.VERTICAL
        addChangeListener {
            panel.scale = this.value.toDouble() / 100.0
            label.text = "Zoom\n${this.value}%"
            redraw()
        }
    }

    private fun resized() {
        panel.parentSize = scrollPane.viewport.size
        revalidate()
        panel.repaint()
    }

    public var currentDiagram: Diagram
        get() = panel.diagram
        set(value) {
            panel.diagram = value
            // logger.debug { "set diagram " }
            redraw()
        }

    private val label = JTextArea("Zoom\n100%").apply {
        font = FontManager.smallItalic
        insets.left = 20
        isEditable = false
        border = BorderFactory.createEmptyBorder(5, 5, 5, 5)
        isOpaque = false
    }

    private val zoomPanel = JPanel(BorderLayout()).apply {
        add(label, BorderLayout.WEST)
        add(scaleSlider, BorderLayout.CENTER)
    }

    public fun zoomSliderOnLeft() {
        zoomPanel.add(label, BorderLayout.NORTH)
        scaleSlider.orientation = JSlider.VERTICAL
        add(zoomPanel, BorderLayout.WEST)
    }

    public fun zoomSliderOnTop() {
        zoomPanel.add(label, BorderLayout.WEST)
        scaleSlider.orientation = JSlider.HORIZONTAL
        add(zoomPanel, BorderLayout.NORTH)
    }

    init {
        currentDiagram = diagram
        add(scrollPane, BorderLayout.CENTER)
        add(zoomPanel, BorderLayout.NORTH)
        addComponentListener(object : ComponentAdapter() {
            override fun componentShown(e: ComponentEvent?) {
                resized()
            }
        })

        zoomSliderOnLeft()
    }

    public fun redraw() {
        panel.redraw()
    }
}
